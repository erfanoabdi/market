package com.sabaos.market;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.fdroid.fdroid.R;
import org.fdroid.fdroid.views.BannerUpdatingRepos;
import org.fdroid.fdroid.views.updates.UpdatesAdapter;
import org.fdroid.fdroid.views.updates.UpdatesItemTouchCallback;

import java.util.List;

import static com.sabaos.market.SecurityViewBinder.securityActivity;
import static org.fdroid.fdroid.views.main.MainActivity.globalContext;

// This adapter generates and displays cards for SecurityViewBinder class

public class SecurityEventsAdapter extends RecyclerView.Adapter<SecurityEventsAdapter.SecurityEventViewHolder> {

    private List<SecurityEvent> securityEventList;

    public SecurityEventsAdapter(List<SecurityEvent> securityEventList) {
        this.securityEventList = securityEventList;
    }

    @NonNull
    @Override
    public SecurityEventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.security_event_cardview1, parent, false);

        return new SecurityEventViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final SecurityEventViewHolder holder, int position) {

        final SecurityEvent securityEvent = securityEventList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (securityEvent.getType().equalsIgnoreCase("quickInstall")) {
                    Intent intent = new Intent(globalContext, QuickInstallActivity.class);
                    securityActivity.startActivity(intent);
                }
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            holder.body.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
        }
        StringBuilder body = new StringBuilder().append(globalContext.getString(R.string.attention_body_p1))
                .append(" ")
                .append(securityEvent.getAppName())
                .append(" ")
                .append(globalContext.getString(R.string.attention_body_p2));
        switch (securityEvent.getType()) {

            case "updates":
                holder.updatesLayout.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.body.setVisibility(View.GONE);
                holder.icon.setVisibility(View.GONE);
                holder.list.setVisibility(View.VISIBLE);
                holder.adapter.registerAdapterDataObserver(holder.adapterChangeListener);
                holder.list.setHasFixedSize(true);
                holder.list.setLayoutManager(new LinearLayoutManager(securityActivity));
                holder.list.setAdapter(holder.adapter);

                ItemTouchHelper touchHelper = new ItemTouchHelper(new UpdatesItemTouchCallback(securityActivity, holder.adapter));
                touchHelper.attachToRecyclerView(holder.list);
                holder.adapter.setIsActive();

                break;
            case "noInternet":
                holder.title.setVisibility(View.GONE);
                holder.body.setVisibility(View.GONE);
                holder.noInternetTextView.setVisibility(View.VISIBLE);
                holder.noInternetTextView.setText(globalContext.getString(R.string.no_internet));
                holder.icon.setVisibility(View.GONE);
                break;
            case "quickInstall":
                holder.title.setText(securityEvent.getAppName());
                holder.body.setText(securityEvent.getPackageName());
                holder.icon.setImageDrawable(securityEvent.getIcon());
                break;
            case "securityLevel":
                holder.title.setText(securityEvent.getAppName());
                holder.body.setText(securityEvent.getPackageName());
                holder.icon.setImageDrawable(securityEvent.getIcon());
                break;
            case "notif":
                String appName = null;
                if (securityEvent.getAppName().equalsIgnoreCase("Telegram") ||
                        securityEvent.getAppName().equalsIgnoreCase("تلگرام")) {
                    appName = "تلگرام";
                }
                if (securityEvent.getAppName().equalsIgnoreCase("Whatsapp") ||
                        securityEvent.getAppName().equalsIgnoreCase("واتساپ")) {
                    appName = "واتساپ";
                }
                if (securityEvent.getAppName().equalsIgnoreCase("Tor Browser") ||
                        securityEvent.getAppName().equalsIgnoreCase("تور") ||
                        securityEvent.getAppName().equalsIgnoreCase("مرورگر تور")) {
                    appName = "تور";
                }
                if (securityEvent.getAppName().equalsIgnoreCase("Merriam-Webster Dictionary")) {

                }
                StringBuilder title = new StringBuilder();
                if (appName != null) {
                    title.append(globalContext.getString(R.string.warning_for_app))
                            .append(" ")
                            .append(appName);
                } else {
                    title.append(globalContext.getString(R.string.warning_for_app))
                            .append(" ")
                            .append(securityEvent.getAppName());
                }
                holder.title.setText(title);
                holder.title.setTextSize(15);
                holder.body.setText(body.toString());
                holder.icon.setVisibility(View.GONE);
                break;
            case "nonSabaApp":
                holder.title.setText(globalContext.getString(R.string.security_warning));
                StringBuilder description = new StringBuilder();
                description.append(globalContext.getString(R.string.security_warning_description)).append("\n").append("\n");
                description.append(securityEvent.getAppName());
                holder.body.setText(description.toString());
                holder.icon.setVisibility(View.GONE);
                break;
            case "aboutApp":
                holder.title.setVisibility(View.GONE);
                holder.body.setVisibility(View.GONE);
                holder.aboutApp.setVisibility(View.VISIBLE);
                holder.aboutApp.setText(securityEvent.getPackageName());
                holder.aboutApp.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                holder.icon.setVisibility(View.GONE);
                holder.aboutApp.setTextColor(globalContext.getResources().getColor(R.color.about_app_color, null));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return securityEventList.size();
    }


    public static class SecurityEventViewHolder extends RecyclerView.ViewHolder {
        protected TextView title;
        protected TextView body;
        protected ImageView icon;
        protected TextView aboutApp;
        protected TextView noInternetTextView;
        protected final UpdatesAdapter adapter;
        protected final RecyclerView list;
        protected final TextView emptyState;
        protected final RecyclerView.AdapterDataObserver adapterChangeListener;
        protected BannerUpdatingRepos bannerUpdatingRepos;
        protected LinearLayout updatesLayout;
        protected RelativeLayout cardViewLayout;

        public SecurityEventViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.security_event_title);
            body = (TextView) view.findViewById(R.id.security_event_body);
            icon = (ImageView) view.findViewById(R.id.security_event_image);
            aboutApp = (TextView) view.findViewById(R.id.about_app);
            noInternetTextView = (TextView) view.findViewById(R.id.no_internet_textview);
//            frameLayout = (FrameLayout) view.findViewById(R.id.updates_frame)
            updatesLayout = (LinearLayout) view.findViewById(R.id.updates_layout);
            bannerUpdatingRepos = (BannerUpdatingRepos) view.findViewById(R.id.banner_updating_repos);
            cardViewLayout = (RelativeLayout) view.findViewById(R.id.cardview_layout);

            adapterChangeListener = new RecyclerView.AdapterDataObserver() {
                @Override
                public void onChanged() {
                    if (adapter.getItemCount() == 0) {

                    } else {
                        cardViewLayout.setVisibility(View.VISIBLE);
                        list.setVisibility(View.VISIBLE);
                        emptyState.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {
                    if (adapter.getItemCount() == 0) {
                        list.setVisibility(View.GONE);
                        emptyState.setVisibility(View.VISIBLE);
                    } else {
                        list.setVisibility(View.VISIBLE);
                        emptyState.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onItemRangeRemoved(int positionStart, int itemCount) {
                    if (adapter.getItemCount() == 0) {
                        list.setVisibility(View.GONE);
                        emptyState.setVisibility(View.VISIBLE);
                    } else {
                        list.setVisibility(View.VISIBLE);
                        emptyState.setVisibility(View.GONE);
                    }
                }
            };

            adapter = new UpdatesAdapter(securityActivity);
            list = (RecyclerView) view.findViewById(R.id.list);
            emptyState = (TextView) view.findViewById(R.id.empty_state);
        }
    }
}
