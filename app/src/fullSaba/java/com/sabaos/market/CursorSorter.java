package com.sabaos.market;

import android.database.Cursor;
import android.database.MatrixCursor;

// Used to reorganize search results to a more logical sort

public class CursorSorter {

    //This method receives the cursor from application and sorts the applications in a more
    // logical order
    public static Cursor sortCursor(Cursor cursor, String searchTerm) {

        MatrixCursor foundInTitle = new MatrixCursor(new String[]{"_id", "rowId", "repoId",
                "compatible", "name", "summary", "icon", "description", "whatsNew", "license",
                "author", "email", "webUrl", "trackerUrl", "sourceUrl", "translation", "video",
                "changeLogUrl", "donateUrl", "bitcoinAddr", "litecoinAddr", "flattrId",
                "liberapayId", "upstreamVersion", "upstreamVercode", "added", "lastUpdated",
                "antiFeatures", "requirements", "iconUrl", "featureGraphic", "promoGraphic",
                "tvBanner", "phoneScreenshots", "sevenInchScreenshots", "tenInchScreenshots",
                "tvScreenshots", "wearScreenshots", "preferredSigner", "suggestedVercode", "isApk",
                "isLocalized", "suggestedApkVersion", "installedVersionCode", "installedVersionName",
                "installedSig", "package_packageName"});
        MatrixCursor foundInSummary = new MatrixCursor(new String[]{"_id", "rowId", "repoId",
                "compatible", "name", "summary", "icon", "description", "whatsNew", "license",
                "author", "email", "webUrl", "trackerUrl", "sourceUrl", "translation", "video",
                "changeLogUrl", "donateUrl", "bitcoinAddr", "litecoinAddr", "flattrId",
                "liberapayId", "upstreamVersion", "upstreamVercode", "added", "lastUpdated",
                "antiFeatures", "requirements", "iconUrl", "featureGraphic", "promoGraphic",
                "tvBanner", "phoneScreenshots", "sevenInchScreenshots", "tenInchScreenshots",
                "tvScreenshots", "wearScreenshots", "preferredSigner", "suggestedVercode", "isApk",
                "isLocalized", "suggestedApkVersion", "installedVersionCode", "installedVersionName",
                "installedSig", "package_packageName"});
        MatrixCursor foundInDesc = new MatrixCursor(new String[]{"_id", "rowId", "repoId",
                "compatible", "name", "summary", "icon", "description", "whatsNew", "license",
                "author", "email", "webUrl", "trackerUrl", "sourceUrl", "translation", "video",
                "changeLogUrl", "donateUrl", "bitcoinAddr", "litecoinAddr", "flattrId",
                "liberapayId", "upstreamVersion", "upstreamVercode", "added", "lastUpdated",
                "antiFeatures", "requirements", "iconUrl", "featureGraphic", "promoGraphic",
                "tvBanner", "phoneScreenshots", "sevenInchScreenshots", "tenInchScreenshots",
                "tvScreenshots", "wearScreenshots", "preferredSigner", "suggestedVercode", "isApk",
                "isLocalized", "suggestedApkVersion", "installedVersionCode", "installedVersionName",
                "installedSig", "package_packageName"});
        MatrixCursor extra = new MatrixCursor(new String[]{"_id", "rowId", "repoId",
                "compatible", "name", "summary", "icon", "description", "whatsNew", "license",
                "author", "email", "webUrl", "trackerUrl", "sourceUrl", "translation", "video",
                "changeLogUrl", "donateUrl", "bitcoinAddr", "litecoinAddr", "flattrId",
                "liberapayId", "upstreamVersion", "upstreamVercode", "added", "lastUpdated",
                "antiFeatures", "requirements", "iconUrl", "featureGraphic", "promoGraphic",
                "tvBanner", "phoneScreenshots", "sevenInchScreenshots", "tenInchScreenshots",
                "tvScreenshots", "wearScreenshots", "preferredSigner", "suggestedVercode", "isApk",
                "isLocalized", "suggestedApkVersion", "installedVersionCode", "installedVersionName",
                "installedSig", "package_packageName"});
        MatrixCursor result = new MatrixCursor(new String[]{"_id", "rowId", "repoId",
                "compatible", "name", "summary", "icon", "description", "whatsNew", "license",
                "author", "email", "webUrl", "trackerUrl", "sourceUrl", "translation", "video",
                "changeLogUrl", "donateUrl", "bitcoinAddr", "litecoinAddr", "flattrId",
                "liberapayId", "upstreamVersion", "upstreamVercode", "added", "lastUpdated",
                "antiFeatures", "requirements", "iconUrl", "featureGraphic", "promoGraphic",
                "tvBanner", "phoneScreenshots", "sevenInchScreenshots", "tenInchScreenshots",
                "tvScreenshots", "wearScreenshots", "preferredSigner", "suggestedVercode", "isApk",
                "isLocalized", "suggestedApkVersion", "installedVersionCode", "installedVersionName",
                "installedSig", "package_packageName"});

        if (cursor.moveToFirst()) {
            do {
                String _id = cursor.getString(cursor.getColumnIndex("_id"));
                String rowId = cursor.getString(cursor.getColumnIndex("rowid"));
                String repoId = cursor.getString(cursor.getColumnIndex("repoId"));
                String compatible = cursor.getString(cursor.getColumnIndex("compatible"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String summary = cursor.getString(cursor.getColumnIndex("summary"));
                String icon = cursor.getString(cursor.getColumnIndex("icon"));
                String description = cursor.getString(cursor.getColumnIndex("description"));
                String whatsNew = cursor.getString(cursor.getColumnIndex("whatsNew"));
                String license = cursor.getString(cursor.getColumnIndex("license"));
                String author = cursor.getString(cursor.getColumnIndex("author"));
                String email = cursor.getString(cursor.getColumnIndex("email"));
                String webUrl = cursor.getString(cursor.getColumnIndex("webURL"));
                String trackerUrl = cursor.getString(cursor.getColumnIndex("trackerURL"));
                String sourceUrl = cursor.getString(cursor.getColumnIndex("sourceURL"));
                String translation = cursor.getString(cursor.getColumnIndex("translation"));
                String video = cursor.getString(cursor.getColumnIndex("video"));
                String changeLogUrl = cursor.getString(cursor.getColumnIndex("changelogURL"));
                String donateUrl = cursor.getString(cursor.getColumnIndex("donateURL"));
                String bitcoinAddr = cursor.getString(cursor.getColumnIndex("bitcoinAddr"));
                String litecoinAddr = cursor.getString(cursor.getColumnIndex("litecoinAddr"));
                String flattrId = cursor.getString(cursor.getColumnIndex("flattrID"));
                String liberapayId = cursor.getString(cursor.getColumnIndex("liberapayID"));
                String upstreamVersion = cursor.getString(cursor.getColumnIndex("upstreamVersion"));
                String upstreamVercode = cursor.getString(cursor.getColumnIndex("upstreamVercode"));
                String added = cursor.getString(cursor.getColumnIndex("added"));
                String lastUpdated = cursor.getString(cursor.getColumnIndex("lastUpdated"));
                String antiFeatures = cursor.getString(cursor.getColumnIndex("antiFeatures"));
                String requirements = cursor.getString(cursor.getColumnIndex("requirements"));
                String iconUrl = cursor.getString(cursor.getColumnIndex("iconUrl"));
                String featureGraphic = cursor.getString(cursor.getColumnIndex("featureGraphic"));
                String promoGraphic = cursor.getString(cursor.getColumnIndex("promoGraphic"));
                String tvBanner = cursor.getString(cursor.getColumnIndex("tvBanner"));
                String phoneScreenshots = cursor.getString(cursor.getColumnIndex("phoneScreenshots"));
                String sevenInchScreenshots = cursor.getString(cursor.getColumnIndex("sevenInchScreenshots"));
                String tenInchScreenshots = cursor.getString(cursor.getColumnIndex("tenInchScreenshots"));
                String tvScreenshots = cursor.getString(cursor.getColumnIndex("tvScreenshots"));
                String wearScreenshots = cursor.getString(cursor.getColumnIndex("wearScreenshots"));
                String preferredSigner = cursor.getString(cursor.getColumnIndex("preferredSigner"));
                String suggestedVercode = cursor.getString(cursor.getColumnIndex("suggestedVercode"));
                String isApk = cursor.getString(cursor.getColumnIndex("isApk"));
                String isLocalized = cursor.getString(cursor.getColumnIndex("isLocalized"));
                String suggestedApkVersion = cursor.getString(cursor.getColumnIndex("suggestedApkVersion"));
                String installedVersionCode = cursor.getString(cursor.getColumnIndex("installedVersionCode"));
                String installedVersionName = cursor.getString(cursor.getColumnIndex("installedVersionName"));
                String installedSig = cursor.getString(cursor.getColumnIndex("installedSig"));
                String package_packageName = cursor.getString(cursor.getColumnIndex("package_packageName"));

                boolean entryAdded = false;
                if (!entryAdded) {
                    for (int i = 0; i < name.length() - searchTerm.length(); i++) {
                        if (name.substring(i, searchTerm.length() + i).equalsIgnoreCase(searchTerm)) {
                            if (i == 0 || name.substring(i - 1, i).equalsIgnoreCase(" ")) {
                                foundInTitle.addRow(new Object[]{_id, rowId, repoId, compatible, name, summary, icon, description, whatsNew, license,
                                        author, email, webUrl, trackerUrl, sourceUrl, translation, video, changeLogUrl,
                                        donateUrl, bitcoinAddr, litecoinAddr, flattrId, liberapayId, upstreamVersion,
                                        upstreamVercode, added, lastUpdated, antiFeatures, requirements, iconUrl, featureGraphic,
                                        promoGraphic, tvBanner, phoneScreenshots, sevenInchScreenshots, tenInchScreenshots,
                                        tvScreenshots, wearScreenshots, preferredSigner, suggestedVercode, isApk, isLocalized,
                                        suggestedApkVersion, installedVersionCode, installedVersionName, installedSig,
                                        package_packageName});
                                entryAdded = true;
                                break;
                            }
                        }
                    }
                }
                if (!entryAdded) {
                    for (int i = 0; i < summary.length() - searchTerm.length(); i++) {
                        if (summary.substring(i, searchTerm.length() + i).equalsIgnoreCase(searchTerm)) {
                            if (i == 0 || summary.substring(i - 1, i).equalsIgnoreCase(" ")) {
                                foundInSummary.addRow(new Object[]{_id, rowId, repoId, compatible, name, summary, icon, description, whatsNew, license,
                                        author, email, webUrl, trackerUrl, sourceUrl, translation, video, changeLogUrl,
                                        donateUrl, bitcoinAddr, litecoinAddr, flattrId, liberapayId, upstreamVersion,
                                        upstreamVercode, added, lastUpdated, antiFeatures, requirements, iconUrl, featureGraphic,
                                        promoGraphic, tvBanner, phoneScreenshots, sevenInchScreenshots, tenInchScreenshots,
                                        tvScreenshots, wearScreenshots, preferredSigner, suggestedVercode, isApk, isLocalized,
                                        suggestedApkVersion, installedVersionCode, installedVersionName, installedSig,
                                        package_packageName});
                                entryAdded = true;
                                break;
                            }
                        }
                    }
                }
                if (!entryAdded) {
                    for (int i = 0; i < description.length() - searchTerm.length(); i++) {
                        if (description.substring(i, searchTerm.length() + i).equalsIgnoreCase(searchTerm)) {
                            if (i == 0 || description.substring(i - 1, i).equalsIgnoreCase(" ")) {
                                foundInDesc.addRow(new Object[]{_id, rowId, repoId, compatible, name, summary, icon, description, whatsNew, license,
                                        author, email, webUrl, trackerUrl, sourceUrl, translation, video, changeLogUrl,
                                        donateUrl, bitcoinAddr, litecoinAddr, flattrId, liberapayId, upstreamVersion,
                                        upstreamVercode, added, lastUpdated, antiFeatures, requirements, iconUrl, featureGraphic,
                                        promoGraphic, tvBanner, phoneScreenshots, sevenInchScreenshots, tenInchScreenshots,
                                        tvScreenshots, wearScreenshots, preferredSigner, suggestedVercode, isApk, isLocalized,
                                        suggestedApkVersion, installedVersionCode, installedVersionName, installedSig,
                                        package_packageName});
                                entryAdded = true;
                                break;
                            }
                        }
                    }
                }
            } while (cursor.moveToNext());
        }
        if (foundInTitle.moveToFirst()) {
            do {
                String _id = foundInTitle.getString(foundInTitle.getColumnIndex("_id"));
                String rowId = foundInTitle.getString(foundInTitle.getColumnIndex("rowid"));
                String repoId = foundInTitle.getString(foundInTitle.getColumnIndex("repoId"));
                String compatible = foundInTitle.getString(foundInTitle.getColumnIndex("compatible"));
                String name = foundInTitle.getString(foundInTitle.getColumnIndex("name"));
                String summary = foundInTitle.getString(foundInTitle.getColumnIndex("summary"));
                String icon = foundInTitle.getString(foundInTitle.getColumnIndex("icon"));
                String description = foundInTitle.getString(foundInTitle.getColumnIndex("description"));
                String whatsNew = foundInTitle.getString(foundInTitle.getColumnIndex("whatsNew"));
                String license = foundInTitle.getString(foundInTitle.getColumnIndex("license"));
                String author = foundInTitle.getString(foundInTitle.getColumnIndex("author"));
                String email = foundInTitle.getString(foundInTitle.getColumnIndex("email"));
                String webUrl = foundInTitle.getString(foundInTitle.getColumnIndex("webURL"));
                String trackerUrl = foundInTitle.getString(foundInTitle.getColumnIndex("trackerURL"));
                String sourceUrl = foundInTitle.getString(foundInTitle.getColumnIndex("sourceURL"));
                String translation = foundInTitle.getString(foundInTitle.getColumnIndex("translation"));
                String video = foundInTitle.getString(foundInTitle.getColumnIndex("video"));
                String changeLogUrl = foundInTitle.getString(foundInTitle.getColumnIndex("changelogURL"));
                String donateUrl = foundInTitle.getString(foundInTitle.getColumnIndex("donateURL"));
                String bitcoinAddr = foundInTitle.getString(foundInTitle.getColumnIndex("bitcoinAddr"));
                String litecoinAddr = foundInTitle.getString(foundInTitle.getColumnIndex("litecoinAddr"));
                String flattrId = foundInTitle.getString(foundInTitle.getColumnIndex("flattrID"));
                String liberapayId = foundInTitle.getString(foundInTitle.getColumnIndex("liberapayID"));
                String upstreamVersion = foundInTitle.getString(foundInTitle.getColumnIndex("upstreamVersion"));
                String upstreamVercode = foundInTitle.getString(foundInTitle.getColumnIndex("upstreamVercode"));
                String added = foundInTitle.getString(foundInTitle.getColumnIndex("added"));
                String lastUpdated = foundInTitle.getString(foundInTitle.getColumnIndex("lastUpdated"));
                String antiFeatures = foundInTitle.getString(foundInTitle.getColumnIndex("antiFeatures"));
                String requirements = foundInTitle.getString(foundInTitle.getColumnIndex("requirements"));
                String iconUrl = foundInTitle.getString(foundInTitle.getColumnIndex("iconUrl"));
                String featureGraphic = foundInTitle.getString(foundInTitle.getColumnIndex("featureGraphic"));
                String promoGraphic = foundInTitle.getString(foundInTitle.getColumnIndex("promoGraphic"));
                String tvBanner = foundInTitle.getString(foundInTitle.getColumnIndex("tvBanner"));
                String phoneScreenshots = foundInTitle.getString(foundInTitle.getColumnIndex("phoneScreenshots"));
                String sevenInchScreenshots = foundInTitle.getString(foundInTitle.getColumnIndex("sevenInchScreenshots"));
                String tenInchScreenshots = foundInTitle.getString(foundInTitle.getColumnIndex("tenInchScreenshots"));
                String tvScreenshots = foundInTitle.getString(foundInTitle.getColumnIndex("tvScreenshots"));
                String wearScreenshots = foundInTitle.getString(foundInTitle.getColumnIndex("wearScreenshots"));
                String preferredSigner = foundInTitle.getString(foundInTitle.getColumnIndex("preferredSigner"));
                String suggestedVercode = foundInTitle.getString(foundInTitle.getColumnIndex("suggestedVercode"));
                String isApk = foundInTitle.getString(foundInTitle.getColumnIndex("isApk"));
                String isLocalized = foundInTitle.getString(foundInTitle.getColumnIndex("isLocalized"));
                String suggestedApkVersion = foundInTitle.getString(foundInTitle.getColumnIndex("suggestedApkVersion"));
                String installedVersionCode = foundInTitle.getString(foundInTitle.getColumnIndex("installedVersionCode"));
                String installedVersionName = foundInTitle.getString(foundInTitle.getColumnIndex("installedVersionName"));
                String installedSig = foundInTitle.getString(foundInTitle.getColumnIndex("installedSig"));
                String package_packageName = foundInTitle.getString(foundInTitle.getColumnIndex("package_packageName"));

                result.addRow(new Object[]{_id, rowId, repoId, compatible, name, summary, icon, description, whatsNew, license,
                        author, email, webUrl, trackerUrl, sourceUrl, translation, video, changeLogUrl,
                        donateUrl, bitcoinAddr, litecoinAddr, flattrId, liberapayId, upstreamVersion,
                        upstreamVercode, added, lastUpdated, antiFeatures, requirements, iconUrl, featureGraphic,
                        promoGraphic, tvBanner, phoneScreenshots, sevenInchScreenshots, tenInchScreenshots,
                        tvScreenshots, wearScreenshots, preferredSigner, suggestedVercode, isApk, isLocalized,
                        suggestedApkVersion, installedVersionCode, installedVersionName, installedSig,
                        package_packageName});


            } while (foundInTitle.moveToNext());
        }
        if (foundInSummary.moveToFirst()) {
            do {
                String _id = foundInSummary.getString(foundInSummary.getColumnIndex("_id"));
                String rowId = foundInSummary.getString(foundInSummary.getColumnIndex("rowid"));
                String repoId = foundInSummary.getString(foundInSummary.getColumnIndex("repoId"));
                String compatible = foundInSummary.getString(foundInSummary.getColumnIndex("compatible"));
                String name = foundInSummary.getString(foundInSummary.getColumnIndex("name"));
                String summary = foundInSummary.getString(foundInSummary.getColumnIndex("summary"));
                String icon = foundInSummary.getString(foundInSummary.getColumnIndex("icon"));
                String description = foundInSummary.getString(foundInSummary.getColumnIndex("description"));
                String whatsNew = foundInSummary.getString(foundInSummary.getColumnIndex("whatsNew"));
                String license = foundInSummary.getString(foundInSummary.getColumnIndex("license"));
                String author = foundInSummary.getString(foundInSummary.getColumnIndex("author"));
                String email = foundInSummary.getString(foundInSummary.getColumnIndex("email"));
                String webUrl = foundInSummary.getString(foundInSummary.getColumnIndex("webURL"));
                String trackerUrl = foundInSummary.getString(foundInSummary.getColumnIndex("trackerURL"));
                String sourceUrl = foundInSummary.getString(foundInSummary.getColumnIndex("sourceURL"));
                String translation = foundInSummary.getString(foundInSummary.getColumnIndex("translation"));
                String video = foundInSummary.getString(foundInSummary.getColumnIndex("video"));
                String changeLogUrl = foundInSummary.getString(foundInSummary.getColumnIndex("changelogURL"));
                String donateUrl = foundInSummary.getString(foundInSummary.getColumnIndex("donateURL"));
                String bitcoinAddr = foundInSummary.getString(foundInSummary.getColumnIndex("bitcoinAddr"));
                String litecoinAddr = foundInSummary.getString(foundInSummary.getColumnIndex("litecoinAddr"));
                String flattrId = foundInSummary.getString(foundInSummary.getColumnIndex("flattrID"));
                String liberapayId = foundInSummary.getString(foundInSummary.getColumnIndex("liberapayID"));
                String upstreamVersion = foundInSummary.getString(foundInSummary.getColumnIndex("upstreamVersion"));
                String upstreamVercode = foundInSummary.getString(foundInSummary.getColumnIndex("upstreamVercode"));
                String added = foundInSummary.getString(foundInSummary.getColumnIndex("added"));
                String lastUpdated = foundInSummary.getString(foundInSummary.getColumnIndex("lastUpdated"));
                String antiFeatures = foundInSummary.getString(foundInSummary.getColumnIndex("antiFeatures"));
                String requirements = foundInSummary.getString(foundInSummary.getColumnIndex("requirements"));
                String iconUrl = foundInSummary.getString(foundInSummary.getColumnIndex("iconUrl"));
                String featureGraphic = foundInSummary.getString(foundInSummary.getColumnIndex("featureGraphic"));
                String promoGraphic = foundInSummary.getString(foundInSummary.getColumnIndex("promoGraphic"));
                String tvBanner = foundInSummary.getString(foundInSummary.getColumnIndex("tvBanner"));
                String phoneScreenshots = foundInSummary.getString(foundInSummary.getColumnIndex("phoneScreenshots"));
                String sevenInchScreenshots = foundInSummary.getString(foundInSummary.getColumnIndex("sevenInchScreenshots"));
                String tenInchScreenshots = foundInSummary.getString(foundInSummary.getColumnIndex("tenInchScreenshots"));
                String tvScreenshots = foundInSummary.getString(foundInSummary.getColumnIndex("tvScreenshots"));
                String wearScreenshots = foundInSummary.getString(foundInSummary.getColumnIndex("wearScreenshots"));
                String preferredSigner = foundInSummary.getString(foundInSummary.getColumnIndex("preferredSigner"));
                String suggestedVercode = foundInSummary.getString(foundInSummary.getColumnIndex("suggestedVercode"));
                String isApk = foundInSummary.getString(foundInSummary.getColumnIndex("isApk"));
                String isLocalized = foundInSummary.getString(foundInSummary.getColumnIndex("isLocalized"));
                String suggestedApkVersion = foundInSummary.getString(foundInSummary.getColumnIndex("suggestedApkVersion"));
                String installedVersionCode = foundInSummary.getString(foundInSummary.getColumnIndex("installedVersionCode"));
                String installedVersionName = foundInSummary.getString(foundInSummary.getColumnIndex("installedVersionName"));
                String installedSig = foundInSummary.getString(foundInSummary.getColumnIndex("installedSig"));
                String package_packageName = foundInSummary.getString(foundInSummary.getColumnIndex("package_packageName"));

                result.addRow(new Object[]{_id, rowId, repoId, compatible, name, summary, icon, description, whatsNew, license,
                        author, email, webUrl, trackerUrl, sourceUrl, translation, video, changeLogUrl,
                        donateUrl, bitcoinAddr, litecoinAddr, flattrId, liberapayId, upstreamVersion,
                        upstreamVercode, added, lastUpdated, antiFeatures, requirements, iconUrl, featureGraphic,
                        promoGraphic, tvBanner, phoneScreenshots, sevenInchScreenshots, tenInchScreenshots,
                        tvScreenshots, wearScreenshots, preferredSigner, suggestedVercode, isApk, isLocalized,
                        suggestedApkVersion, installedVersionCode, installedVersionName, installedSig,
                        package_packageName});


            } while (foundInSummary.moveToNext());
        }
        if (foundInDesc.moveToFirst()) {
            do {
                String _id = foundInDesc.getString(foundInDesc.getColumnIndex("_id"));
                String rowId = foundInDesc.getString(foundInDesc.getColumnIndex("rowid"));
                String repoId = foundInDesc.getString(foundInDesc.getColumnIndex("repoId"));
                String compatible = foundInDesc.getString(foundInDesc.getColumnIndex("compatible"));
                String name = foundInDesc.getString(foundInDesc.getColumnIndex("name"));
                String summary = foundInDesc.getString(foundInDesc.getColumnIndex("summary"));
                String icon = foundInDesc.getString(foundInDesc.getColumnIndex("icon"));
                String description = foundInDesc.getString(foundInDesc.getColumnIndex("description"));
                String whatsNew = foundInDesc.getString(foundInDesc.getColumnIndex("whatsNew"));
                String license = foundInDesc.getString(foundInDesc.getColumnIndex("license"));
                String author = foundInDesc.getString(foundInDesc.getColumnIndex("author"));
                String email = foundInDesc.getString(foundInDesc.getColumnIndex("email"));
                String webUrl = foundInDesc.getString(foundInDesc.getColumnIndex("webURL"));
                String trackerUrl = foundInDesc.getString(foundInDesc.getColumnIndex("trackerURL"));
                String sourceUrl = foundInDesc.getString(foundInDesc.getColumnIndex("sourceURL"));
                String translation = foundInDesc.getString(foundInDesc.getColumnIndex("translation"));
                String video = foundInDesc.getString(foundInDesc.getColumnIndex("video"));
                String changeLogUrl = foundInDesc.getString(foundInDesc.getColumnIndex("changelogURL"));
                String donateUrl = foundInDesc.getString(foundInDesc.getColumnIndex("donateURL"));
                String bitcoinAddr = foundInDesc.getString(foundInDesc.getColumnIndex("bitcoinAddr"));
                String litecoinAddr = foundInDesc.getString(foundInDesc.getColumnIndex("litecoinAddr"));
                String flattrId = foundInDesc.getString(foundInDesc.getColumnIndex("flattrID"));
                String liberapayId = foundInDesc.getString(foundInDesc.getColumnIndex("liberapayID"));
                String upstreamVersion = foundInDesc.getString(foundInDesc.getColumnIndex("upstreamVersion"));
                String upstreamVercode = foundInDesc.getString(foundInDesc.getColumnIndex("upstreamVercode"));
                String added = foundInDesc.getString(foundInDesc.getColumnIndex("added"));
                String lastUpdated = foundInDesc.getString(foundInDesc.getColumnIndex("lastUpdated"));
                String antiFeatures = foundInDesc.getString(foundInDesc.getColumnIndex("antiFeatures"));
                String requirements = foundInDesc.getString(foundInDesc.getColumnIndex("requirements"));
                String iconUrl = foundInDesc.getString(foundInDesc.getColumnIndex("iconUrl"));
                String featureGraphic = foundInDesc.getString(foundInDesc.getColumnIndex("featureGraphic"));
                String promoGraphic = foundInDesc.getString(foundInDesc.getColumnIndex("promoGraphic"));
                String tvBanner = foundInDesc.getString(foundInDesc.getColumnIndex("tvBanner"));
                String phoneScreenshots = foundInDesc.getString(foundInDesc.getColumnIndex("phoneScreenshots"));
                String sevenInchScreenshots = foundInDesc.getString(foundInDesc.getColumnIndex("sevenInchScreenshots"));
                String tenInchScreenshots = foundInDesc.getString(foundInDesc.getColumnIndex("tenInchScreenshots"));
                String tvScreenshots = foundInDesc.getString(foundInDesc.getColumnIndex("tvScreenshots"));
                String wearScreenshots = foundInDesc.getString(foundInDesc.getColumnIndex("wearScreenshots"));
                String preferredSigner = foundInDesc.getString(foundInDesc.getColumnIndex("preferredSigner"));
                String suggestedVercode = foundInDesc.getString(foundInDesc.getColumnIndex("suggestedVercode"));
                String isApk = foundInDesc.getString(foundInDesc.getColumnIndex("isApk"));
                String isLocalized = foundInDesc.getString(foundInDesc.getColumnIndex("isLocalized"));
                String suggestedApkVersion = foundInDesc.getString(foundInDesc.getColumnIndex("suggestedApkVersion"));
                String installedVersionCode = foundInDesc.getString(foundInDesc.getColumnIndex("installedVersionCode"));
                String installedVersionName = foundInDesc.getString(foundInDesc.getColumnIndex("installedVersionName"));
                String installedSig = foundInDesc.getString(foundInDesc.getColumnIndex("installedSig"));
                String package_packageName = foundInDesc.getString(foundInDesc.getColumnIndex("package_packageName"));

                result.addRow(new Object[]{_id, rowId, repoId, compatible, name, summary, icon, description, whatsNew, license,
                        author, email, webUrl, trackerUrl, sourceUrl, translation, video, changeLogUrl,
                        donateUrl, bitcoinAddr, litecoinAddr, flattrId, liberapayId, upstreamVersion,
                        upstreamVercode, added, lastUpdated, antiFeatures, requirements, iconUrl, featureGraphic,
                        promoGraphic, tvBanner, phoneScreenshots, sevenInchScreenshots, tenInchScreenshots,
                        tvScreenshots, wearScreenshots, preferredSigner, suggestedVercode, isApk, isLocalized,
                        suggestedApkVersion, installedVersionCode, installedVersionName, installedSig,
                        package_packageName});


            } while (foundInDesc.moveToNext());
        }
        return result;
    }
}