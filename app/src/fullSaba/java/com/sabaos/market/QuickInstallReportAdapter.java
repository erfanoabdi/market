package com.sabaos.market;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.fdroid.fdroid.R;

import java.util.ArrayList;

import static com.sabaos.market.QuickInstallActivity.appInstallationReports;
import static com.sabaos.market.QuickInstallReport.configuration;
import static org.fdroid.fdroid.views.main.MainActivity.globalContext;

public class QuickInstallReportAdapter extends RecyclerView.Adapter<QuickInstallReportAdapter.QuickInstallDialogViewHolder> {

    public static ArrayList<AppInstallationReport> report = new ArrayList<>();

    @NonNull
    @Override
    public QuickInstallDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quick_install_report_adapter, parent, false);
        report.clear();
        return new QuickInstallDialogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuickInstallDialogViewHolder holder, int position) {

        AppInstallationReport appInstallationReport = appInstallationReports.get(position);
        holder.appName.setText(appInstallationReport.getApp().name);
        if (appInstallationReport.isInstalled()) {
            holder.appState.setText(R.string.app_installed);
        } else {
            int value = appInstallationReport.getErrorCode();
            String description;
            switch (value) {
                case 0:
                    description = globalContext.getString(R.string.app_installed);
                    holder.appState.setTextColor(Color.GREEN);
                    break;
                case 1:
                    description = globalContext.getString(R.string.app_not_installed);
                    break;
                case 2:
                    description = globalContext.getString(R.string.app_install_error);
                    break;
                case 3:
                    description = globalContext.getString(R.string.download_interrupted1);
                    break;
                default:
                    description = globalContext.getString(R.string.download_interrupted1);
                    break;
            }
            holder.appState.setText(description);
        }
        if (appInstallationReport.isInstalled()) {
            holder.imageView.setImageResource(R.drawable.ic_check_circle_24px);
        }

    }

    @Override
    public int getItemCount() {
        return appInstallationReports.size();
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class QuickInstallDialogViewHolder extends RecyclerView.ViewHolder {

        TextView appName;
        TextView appState;
        ImageView imageView;

        public QuickInstallDialogViewHolder(View itemView) {
            super(itemView);
            appName = itemView.findViewById(R.id.app_name);
            appState = itemView.findViewById(R.id.app_state);
            int screenWidthDp = configuration.screenWidthDp;
            appName.setWidth(screenWidthDp * 2 / 7);
            appState.setWidth(screenWidthDp *2  / 7);
            imageView = itemView.findViewById(R.id.image);

        }
    }
}
