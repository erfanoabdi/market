package com.sabaos.market;

import java.util.ArrayList;


// Class used to generate a list of trusted signatures

public class OSApps {

    private OSApps() {
    }

    public static ArrayList<String> generateWhiteList() {

        ArrayList<String> systemAppInfoArrayList = new ArrayList<>();

        // Samsung S9 signatures

        // ابـر سبـا
        systemAppInfoArrayList.add("0ee44d5d311d4e4d86b36914a49cb316");

        // Music
        systemAppInfoArrayList.add("240e78599976892ae840012a74cc155d");

        // Tags,Default Print Service,Bookmark Provider,Calculator,Calendar,Companion Device Manager
        // Clock,Basic Daydreams,Photo Screensavers,Email,Exchange Services,HTML Viewer,One Time Init
        // Print Service Recommendation Service,Print Spooler,Calendar Storage,com.android.smspush
        // Intent Filter Verification Service,Storage Manager,Terminal,com.android.wallpaperpicker
        // Android System WebView,Pico TTS,F-Droid Privileged Extension,cLock,Trebuchet,Updater
        systemAppInfoArrayList.add("29855fe144f2f8cd936dcbfba4f6a720");

        // Firefox Klar
        systemAppInfoArrayList.add("46278c0840bcb12016dae7eb7de26bd8");

        // MTP Host,Download Manager,Downloads,Media Storage
        systemAppInfoArrayList.add("4f711ca3e82f61c3bee29427cf060f79");

        // MuPDF viewer
        systemAppInfoArrayList.add("5f189df9c7482a659154c71fa1c0a45f");

        // RootPA
        systemAppInfoArrayList.add("6ea13124e2533ac9a98e6b82a6a2e64b");

        // TerminalEmulator
        systemAppInfoArrayList.add("716d7b3e9402edf90bb4e02a6c09e969");

        // Hamgamsaz
        systemAppInfoArrayList.add("7fcd881f65f13e790ee3cba704595650");

        // Messaging
        systemAppInfoArrayList.add("88655812e26fedc73eea67541b88dea9");

        // Call Log Backup/Restore,Contacts,Phone,Android Keyboard (AOSP),Blocked Numbers Storage
        // Contacts Storage,User Dictionary
        systemAppInfoArrayList.add("9883b62d6d568531a93346890bbef41d");

        // Android System,Android Services Library,Android Shared Library,com.android.backupconfirm
        // Bluetooth,Bluetooth MIDI Service,CaptivePortalLogin,com.android.carrierconfig,CarrierDefaultApp
        // Cell Broadcasts,Certificate Installer,Package Access Helper,Dev Tools,Files,Android Easter Egg
        // Emergency information,External Storage,Gallery,Input Devices,Key Chain,Fused Location
        // Work profile setup,Messaging service,Nfc Service,Package installer,PacProcessor,Phone Services
        // Settings Storage,Phone and Messaging Storage,ProxyHandler,Call Management,Settings
        // com.android.sharedstoragebackup,Shell,SIM Toolkit,System UI,VpnDialogs,Live Wallpaper Picker
        // com.android.wallpaperbackup,com.android.wallpapercropper,Persian Calendar,Saba System
        // LineageOS Settings,Lineage Settings Storage,Carbon,Blueberry,Cocoa,Cyan,Forest,Pumpkin,Cherry
        // Lavender,Tomato,Banana,org.lineageos.overlay.black,org.lineageos.overlay.dark
        // Profiles Trust Provider,Recorder,Setup Wizard,Weather Provider
        systemAppInfoArrayList.add("d0776b32f9b0da75095b41c3f52e7315");

        // Camera
        systemAppInfoArrayList.add("d47c94dc3518e9b8f7138984088b2ec9");

        // com.android.cts.ctsshim,com.android.cts.priv.ctsshim
        systemAppInfoArrayList.add("e967c9e5fd92879f6bac96358de84d90");

        // Saba Market
        systemAppInfoArrayList.add("ebbee34dff1b07e28392181b9793dd55");

        // SecureSMS
        systemAppInfoArrayList.add("f2ec48d1bf12d3b58835147f3a7295a9");



        // Moto X4 signatures

        // بله
        systemAppInfoArrayList.add("010d2893fb5e65fc3ea6ca5fd9e6d4fb");

        // Cloud
        systemAppInfoArrayList.add("0ee44d5d311d4e4d86b36914a49cb316");

        // تقویم فارسی
        systemAppInfoArrayList.add("52155cfca93a4b2df8224ab6f2b97db5");

        // همگام‌ساز
        systemAppInfoArrayList.add("7fcd881f65f13e790ee3cba704595650");

        // بلد
        systemAppInfoArrayList.add("a8e2b88d7650c7bbadee959192db1769");

        // ‏سیستم Android,android.auto_generated_rro__,Android Services Library
        // Android Shared Library,اطلاعات اضطراری,حافظه خارجی,گالری,Input Devices
        // ,نمایشگر با لبه بریده,نمایشگر با لبه بریده دوتایی,لبه بلند,جاکلیدی,Fused Location
        // ,راه‌اندازی نمایه کاری,سرویس پیام رسانی,‏سرویس Nfc,نصب‌کننده بسته,PacProcessor
        // ,سرویس‌های تلفن,تنظیم محل ذخیره,فضای ذخیره تلفن و پیام‌رسانی,ProxyHandler,SecureElementApplication
        // ,مدیریت تماس,تنظیمات,com.android.sharedstoragebackup
        // Shell,Sim App Dialog,‏ابزار کار SIM,رابط کاربر سیستم,ردیابی سیستم,VpnDialogs,Live Wallpaper Picker
        // com.android.wallpaperbackup,com.android.wallpapercropper,رادیو FM,بازار,com.qti.qualcomm.datastatusnotification,
        // com.qualcomm.qcrilmsgtunnel,com.qualcomm.qti.telephonyservice,com.qualcomm.timeservice,سرويس Wfd
        // com.quicinc.cne.CNEService.CNEServiceApp
        // LineageOS System,org.codeaurora.ims,AudioFX,LineageOS Settings,Lineage Settings Storage,Carbon,Blueberry
        // Cocoa,Cyan,Forest,Pumpkin,Cherry,Lavender,Tomato,Banana,org.lineageos.overlay.black,org.lineageos.overlay.dark
        // Profiles Trust Provider,Recorder,تنظیمات پیشرفته,راهنمای راه‌اندازی,Camera,Weather Provider
        // com.android.backupconfirm,بلوتوث,Bluetooth MIDI Service,CaptivePortalLogin,com.android.carrierconfig
        // CarrierDefaultApp,پخش‌های سلولی,نصب‌کننده گواهی,راهنمای دسترسی به بسته,Dev Tools,Files,PAINT.APK
        systemAppInfoArrayList.add("aa040eac41da625b5251d717a0597345");

        // قرآن
        systemAppInfoArrayList.add("b827446f114ba34e067176ec054ba635");

        // Call Log Backup/Restore,مخاطبین,تلفن,‏صفحه‌کلید Android ‏(AOSP),محل ذخیره شماره‌های مسدود شده
        // حافظه مخاطبین,واژه‌نامه کاربر
        systemAppInfoArrayList.add("b95ad958c969bfbdfe028151edfd53af");

        // Book Reader
        systemAppInfoArrayList.add("d92e5a0e883bcc79cd7233b44af3ebe7");

        // Fennec F-Droid
        systemAppInfoArrayList.add("da03897e192cc766155eadcf73989726");

        // برچسب‌ها,سرویس چاپ پیش‌فرض,Bookmark Provider,ماشین‌حساب,تقویم,مدیر دستگاه مرتبط
        // ساعت,رویاهای ساده,محافظ صفحه نمایش عکس,ایمیل,‏سرویس‌های Exchange,مشاهده گر HTML,Trebuchet,پیام‌رسانی
        // One Time Init,Print Service Recommendation Service,هماهنگ‌کننده چاپ,حافظه تقویم,Settings Suggestions
        // com.android.smspush,Intent Filter Verification Service,مدیریت حافظه,ترمینال,Android System WebView
        // Wallpapers,موسیقی,Browser,cLock,Updater
        systemAppInfoArrayList.add("dfa92a5e00ade5d6bbac204abc72e1a3");

        // ‏میزبان MTP,Download Manager,بارگیری‌ها,فضای ذخیره‌سازی رسانه
        systemAppInfoArrayList.add("e2fd164308c83155bc95dd4afa653c4a");


        // OnePlus A6013 sigs
        systemAppInfoArrayList.add("2bf7425e4b658729f4e80632c4c102ec");
        systemAppInfoArrayList.add("32909e9861d60f2e2592c094ba3e00d5");
        systemAppInfoArrayList.add("3c307023e5c84bca0bcc431e5af7735e");
        systemAppInfoArrayList.add("45dbd7dee0632da08d497d186df668f6");
        systemAppInfoArrayList.add("661f248a1b87a39eb9dc02de80b63eb7");
        systemAppInfoArrayList.add("7189dce4c6bfa0e460f2d138f10bda7d");
        systemAppInfoArrayList.add("807f518d8e966862283a9bbb25054d52");
        systemAppInfoArrayList.add("8209b2bb02643fbea67c52839bab9397");
        systemAppInfoArrayList.add("87739d4671a997a8f1d78ea9476a9a54");
        systemAppInfoArrayList.add("95c4a195fa53db3c740f2f37f87bedfc");
        systemAppInfoArrayList.add("9793a4c40df601b18f55a64281dc8981");
        systemAppInfoArrayList.add("9ffcbca23e155b432b63e13270bdcb8e");
        systemAppInfoArrayList.add("a3a1e461b1d118831f655bfd9b679481");
        systemAppInfoArrayList.add("a5ddeed2036dbf12baaf7170c97f10ae");
        systemAppInfoArrayList.add("a8e2b88d7650c7bbadee959192db1769");
        systemAppInfoArrayList.add("b2039a72aa8deaff9a2595855f8cdb77");
        systemAppInfoArrayList.add("d7902db3cfa25be2aac018648b46f285");
        systemAppInfoArrayList.add("da3cddb9f1162740f4bc71f366b7e91a");
        systemAppInfoArrayList.add("e58ce1fc275e005922a755ec2f69b8a9");

        return systemAppInfoArrayList;
    }
}

