package com.sabaos.market;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.fdroid.fdroid.R;
import org.fdroid.fdroid.data.Apk;
import org.fdroid.fdroid.data.ApkProvider;
import org.fdroid.fdroid.data.App;
import org.fdroid.fdroid.views.AppDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import static org.fdroid.fdroid.views.main.MainActivity.globalContext;
import static org.fdroid.fdroid.views.whatsnew.WhatsNewAdapter.appArrayList;


// This activity helps users install several apps together
// It chooses some hardcoded apps from cursor received from server and displays them to user.


public class QuickInstallActivity extends AppCompatActivity implements OnApkDownloadedListener,
        OnAppInstalledListener {
    public static RecyclerView quickInstallRecyclerView;
    Button button;
    private static int numberOfAppsToBeInstalled = 0;
    private static int counter = -1;
    public static Apk installApk;
    QuickInstallAdapter quickInstallAdapter;
    private static ArrayList<App> appsToInstall = new ArrayList<>();
    private static Context context;
    public static String mutex = "";
    private static AppDetailsActivity appDetailsActivity;
    private static ProgressBar installProgressbar;
    private static int progressBarMaxValue;
    private static TextView progressBarTextView;
    private static TextView progressBarTitle;
    private static LinearLayout progressBarLayout;
    private String reason;
    private static boolean canExitActivity;
    private static String appName;
    private static AppCompatActivity activity;
    public static String installMode = "";
    public static App appToInstall;
    public static ArrayList<AppInstallationReport> appInstallationReports = new ArrayList<>();
    private static boolean isAlreadyRun = false;

    public QuickInstallActivity(String reason) {
        this.reason = reason;
        if (context == null) {
            context = globalContext;
        }
    }

    public QuickInstallActivity() {
        if (context == null) {
            context = globalContext;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_install);
        isAlreadyRun = false;
        context = getApplicationContext();
        activity = this;
        canExitActivity = true;
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.quick_install_actionbar);
        setTitle(getResources().getString(R.string.quick_install_activity_name));
        progressBarTitle = findViewById(R.id.progressbar_title);
        quickInstallAdapter = new QuickInstallAdapter("", 100, appArrayList);
        installProgressbar = (ProgressBar) findViewById(R.id.install_progressbar);
        progressBarTextView = findViewById(R.id.progress_bar_text);
        progressBarLayout = findViewById(R.id.progress_bar_layout);
        quickInstallRecyclerView = findViewById(R.id.app_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        quickInstallRecyclerView.setLayoutManager(linearLayoutManager);
        quickInstallRecyclerView.setAdapter(quickInstallAdapter);
        quickInstallRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        button = findViewById(R.id.quick_install);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appsToInstall.clear();
                isAlreadyRun = false;
                counter = -1;
                appsToInstall = quickInstallAdapter.getAppsToInstall();
                numberOfAppsToBeInstalled = appsToInstall.size();
                progressBarMaxValue = numberOfAppsToBeInstalled;
                installProgressbar.setMax(progressBarMaxValue);
                appInstallationReports.clear();
                if (numberOfAppsToBeInstalled > 0) {
                    for (App app : appsToInstall) {
                        appInstallationReports.add(new AppInstallationReport(app, false, 100));
                    }
                    canExitActivity = false;
                    button.setClickable(false);
                    button.setVisibility(View.GONE);
                    setProgressBarValue(0);
                    quickInstallRecyclerView.setVisibility(View.GONE);
                    progressBarLayout.setVisibility(View.VISIBLE);
                    installProgressbar.setVisibility(View.VISIBLE);
                    progressBarTextView.setVisibility(View.VISIBLE);
                    progressBarTitle.setVisibility(View.VISIBLE);
                    installApp();
                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, getString(R.string.no_app_selected), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }


    // Runs when on Apk is downloaded

    @Override
    public void onDownloaded() {

    }

    // Sets progressbar new value

    private void setProgressBarValue(final int progress) {
        final int value;
        value = progress + 1;
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                installProgressbar.setProgress(value);
                StringBuilder stringBuilder = new StringBuilder();
                int progress = (int) Math.round(value / Double.valueOf(progressBarMaxValue) * 100);
                if (progress > 100) progress = 100;
                stringBuilder.append(String.valueOf(progress));
                stringBuilder.append("%");
                stringBuilder.append(" نصب شده");
                progressBarTextView.setText(stringBuilder.toString());
            }
        });
    }

    // Installs chosen apps

    public void installApp() {

        try {
            if (installProgressbar != null)
                setProgressBarValue(counter);
            counter += 1;
            if (counter < appsToInstall.size()) {
                App app = appsToInstall.get(counter);
                appToInstall = app;
                appName = app.name;
                appDetailsActivity = new AppDetailsActivity(app, "quickInstall", context, activity, new OnQuickInstallAppStatus() {
                    @Override
                    public void onDownload() {
                        String downloading = context.getString(R.string.downloading) + " " + appName;
                        progressBarTitle.setText(downloading);
                    }

                    @Override
                    public void onDownloadError() {
                        if (activity != null) {
                            String downloadError = String.format(context.getString(R.string.download_interrupted), appName);
                            progressBarTitle.setText(downloadError);
                            for (AppInstallationReport appInstallationReport : appInstallationReports) {
                                if (appInstallationReport.getApp().name.equalsIgnoreCase(appToInstall.name)) {
                                    appInstallationReport.setInstalled(false);
                                    appInstallationReport.setErrorCode(3);
                                    break;
                                }
                            }
                            try {
                                checkWebAppInstallations();
                                if (anyAppInstalled()) {
                                    QuickInstallPreferences quickInstallPreferences = new QuickInstallPreferences(globalContext);
                                    quickInstallPreferences.saveData("quickInstall", "true");
                                }
                                canExitActivity = true;
                                installMode = "";
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!isAlreadyRun) {
                                            loadInstallReport();
                                        }
                                        isAlreadyRun = true;
                                    }
                                }, 1000);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onInstall() {
                        String installing = context.getString(R.string.installing) + " " + appName;
                        progressBarTitle.setText(installing);
                    }

                    @Override
                    public void onInstallError() {
                        String installInterrupted = String.format(context.getString(R.string.install_error), appName);
                        progressBarTitle.setText(installInterrupted);
                        for (AppInstallationReport appInstallationReport : appInstallationReports) {
                            if (appInstallationReport.getApp().name.equalsIgnoreCase(appToInstall.name)) {
                                appInstallationReport.setInstalled(false);
                                appInstallationReport.setErrorCode(2);
                                break;
                            }
                        }
                    }

                    @Override
                    public void onInstallComplete() {
                        String installInterrupted = String.format(context.getString(R.string.install_complete), appName);
                        progressBarTitle.setText(installInterrupted);
                        for (AppInstallationReport appInstallationReport : appInstallationReports) {
                            if (appInstallationReport.getApp().name.equalsIgnoreCase(appToInstall.name)) {
                                appInstallationReport.setInstalled(false);
                                appInstallationReport.setErrorCode(0);
                                break;
                            }
                        }
                    }
                });
                Apk apkToInstall = ApkProvider.Helper.findSuggestedApk(context, app);

                //if Market did not find any relevant Apk to install, move on to install the next app
                if (apkToInstall == null) {
                    installApp();
                }
                installApk = apkToInstall;
                appDetailsActivity.onCreate(null, null);
                appDetailsActivity.installApk(apkToInstall);


            } else {
                progressBarTitle.setText(context.getString(R.string.app_install_finished));
                if (installProgressbar != null) setProgressBarValue(progressBarMaxValue);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            checkWebAppInstallations();
                            if (anyAppInstalled()) {
                                QuickInstallPreferences quickInstallPreferences = new QuickInstallPreferences(globalContext);
                                quickInstallPreferences.saveData("quickInstall", "true");
                            }
                            canExitActivity = true;
                            installMode = "";
                            loadInstallReport();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 1000);
            }
        } catch (Exception e) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, context.getString(R.string.unfortuantely) + " " + appName + context.getString(R.string.not_installed), Toast.LENGTH_LONG).show();
                }
            });

            // Whether or not app installed suceeded, move on to install the next app
            onInstalled();
        }
    }


    // This is used to notice Apps that are installed as shortcut.
    // When the shortcut install dialogue is displayed, this activity goes to pause mode, so whether
    // the user presses "add" or "cancel", the "onResume" method is run afterwards. This is where we
    // can continue to install the next app.

    @Override
    public void onResume() {
        super.onResume();
        if (mutex.equalsIgnoreCase("webApp")) {
            installApp();
        }
    }


    // Runs when Apk is installed

    @Override
    public void onInstalled() {

        // if Apk did not install because of reason, display it as a Toast
        if (reason != null) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(globalContext, reason, Toast.LENGTH_LONG).show();
                    Log.e("download error", reason);
                }
            });
        }
        if (mutex.equalsIgnoreCase("apk")) {
            installApp();
        }
    }


    // if the user has chosen some apps to install and has pressed the install button, disable the
    // back button, because we need him to stay on this activity. if not, he can quit. if the user
    // closes the activity and reopens it, he doesn't see the installed apps anymore. He just seen the
    // remaining app from the list which he can choose to install.

    @Override
    public void onBackPressed() {
        if (canExitActivity) {
            super.onBackPressed();
        }
    }

    // This function load a fragment dialog displaying a report containing selected apps' installation
    // state and quits upon pressing OK button.

    public void checkWebAppInstallations() {

        ArrayList<AppInstallationReport> webApps = new ArrayList<>();
        for (AppInstallationReport appInstallationReport : appInstallationReports) {
            int isWebAppInd = appInstallationReport.getApp().description.indexOf("وب اپ ");
            boolean isWebApp = isWebAppInd > 0 && isWebAppInd < 30;
            if (isWebApp) {
                webApps.add(new AppInstallationReport(appInstallationReport.getApp(), false, 100));
            }
        }
        ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
        List<ShortcutInfo> shortcutInfoList = shortcutManager.getPinnedShortcuts();
        SabaSharedPreferences sabaSharedPreferences = new SabaSharedPreferences(context);
        if (shortcutInfoList.size() > 0) {

            for (AppInstallationReport appInstallationReport : webApps) {
                boolean isInstalled = false;
                for (ShortcutInfo shortcutInfo : shortcutInfoList) {
                    if (appInstallationReport.getApp().name.equalsIgnoreCase(shortcutInfo.getId())) {
                        isInstalled = true;
                    }
                }
                if (isInstalled) {
                    sabaSharedPreferences.saveData(appInstallationReport.getApp().name, "true");
                    appInstallationReport.setInstalled(true);
                    appInstallationReport.setErrorCode(0);
                } else {
                    appInstallationReport.setInstalled(false);
                    appInstallationReport.setErrorCode(1);
                }
            }
        } else {
            for (AppInstallationReport appInstallationReport : webApps) {
                appInstallationReport.setInstalled(false);
                appInstallationReport.setErrorCode(1);
            }
        }

        for (AppInstallationReport webApp : webApps) {
            for (AppInstallationReport appInstallationReport : appInstallationReports) {
                if (webApp.getApp().name.equalsIgnoreCase(appInstallationReport.getApp().name)) {
                    appInstallationReport.setInstalled(webApp.isInstalled());
                    appInstallationReport.setErrorCode(webApp.getErrorCode());
                }
            }
        }

    }

    private void loadInstallReport() {

        Intent intent = new Intent(context, QuickInstallReport.class);
        context.startActivity(intent);
        if (activity != null) activity.finish();
    }

    private boolean anyAppInstalled() {
        boolean anyAppInstalled = false;
        for (AppInstallationReport appInstallationReport : appInstallationReports) {
            if (appInstallationReport.isInstalled()) {
                anyAppInstalled = true;
                break;
            }
        }
        return anyAppInstalled;
    }
}