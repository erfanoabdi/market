package com.sabaos.market;

// Callback interface to notify app install

public interface OnAppInstalledListener {

    void onInstalled();
}
