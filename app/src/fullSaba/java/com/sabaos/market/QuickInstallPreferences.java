package com.sabaos.market;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

// Saves if the QuickInstallActivity has run or not

public class QuickInstallPreferences {

    private Context context;
    private SharedPreferences quickInstallPreferences;
    private android.content.SharedPreferences.Editor editor;

    public QuickInstallPreferences(Context context) {
        this.context = context;
        quickInstallPreferences = (android.content.SharedPreferences) context.getSharedPreferences("QuickInstallActivity", Context.MODE_PRIVATE);
        editor = quickInstallPreferences.edit();
    }

    public void saveData(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getData(String key) {
        return quickInstallPreferences.getString(key, null);
    }

    public Map getAll() {
        return quickInstallPreferences.getAll();
    }

    public void removeData(String key) {
        editor.remove(key);
        editor.commit();
    }

}
