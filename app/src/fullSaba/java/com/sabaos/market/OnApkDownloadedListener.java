package com.sabaos.market;

// Callback interface to notify Apk download

public interface OnApkDownloadedListener {

    void onDownloaded();
}
