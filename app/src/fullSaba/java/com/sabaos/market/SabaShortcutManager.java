package com.sabaos.market;

import android.content.Context;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.os.Build;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import static org.fdroid.fdroid.views.main.MainActivity.globalContext;


// It checks installed shortcuts taken from Android against its saved values. If there is a saved
// value that does not correspond to an installed shortcut, it means that the user has deleted the
// shortcut when app was not running, thus we remove the shortcut from saved values.


public class SabaShortcutManager {

    private Context context;

    public SabaShortcutManager(Context context) {
        this.context = context;
    }

    public void checkShortcuts() {
        SabaSharedPreferences sabaSharedPreferences = new SabaSharedPreferences(context);
        if (Build.VERSION.SDK_INT >= 26) {
            ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
            List<ShortcutInfo> shortcutInfoList = shortcutManager.getPinnedShortcuts();
            Map<String, String> allEntries = sabaSharedPreferences.getAll();
            boolean isInstalled = false;
            for (Map.Entry<String, String> entry : allEntries.entrySet()) {
                for (ShortcutInfo shortcutInfo : shortcutInfoList) {
                    if (entry.getKey().equalsIgnoreCase(shortcutInfo.getId())) {
                        isInstalled = true;
                        break;
                    }
                }
                if (!isInstalled) {
                    sabaSharedPreferences.removeData(entry.getKey());
                }
                isInstalled = false;
            }
        } else {
            Toast.makeText(globalContext, "This device does not fully support  this app", Toast.LENGTH_LONG).show();
        }
    }
}
