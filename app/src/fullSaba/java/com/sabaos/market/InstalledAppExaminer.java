package com.sabaos.market;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.fdroid.fdroid.data.DBHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

// This class examines all apps installed on the device and extracts those non-system apps that are
// not installed from SabaMarket. Since this procedure is a bit time-consuming, we have run it on
// a different thread. onTaskFinished() method of the interface is called after the procedure is completed.
// In order to use this class, first use examineDangerousApps() method. This method generates a list of
// non-Saba apps. After that, you can call getNonSabaApps() to retrieve the list of apps.

public class InstalledAppExaminer {

    private static final String INSTALLED_APPS_TABLE_NAME = "fdroid_installedApp";
    private static final String FDROID_APPS_TABLE_NAME = "fdroid_apk";

    private static final String[] FDROID_APPS_COLUMNS = {"sig"};
    private static final String[] INSTALLED_APP_COLUMNS = {"applicationLabel", "sig"};

    private ArrayList<String> nonSabaApps = new ArrayList<>();

    private ArrayList<SecurityEvent> dangerousApps = new ArrayList<>();
    private ArrayList<SecurityEvent> dangerousAppsTemp = new ArrayList<>();


    private ArrayList<SabaMarketApp> installedAppArray = new ArrayList<>();
    private ArrayList<String> secureAppsArray = new ArrayList<>();
    private ArrayList<PackageInfo> installedPackages = new ArrayList<>();
    public static ArrayList<String> nonSabaAppsPackageNames = new ArrayList<>();

    // Generates a list of non-Saba apps

    public void examineDangerousApps(final Context context, final OnTaskExecution onTaskExecution) {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                PackageManager packageManager = context.getPackageManager();
                List<ApplicationInfo> packages = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);

                installedAppArray = getInstalledAppsSigs(context);
                secureAppsArray = getSabaMarketAppsSigs(context);
                ArrayList<String> whiteListsArray = OSApps.generateWhiteList();
                for (String whiteListSignature : whiteListsArray) {
                    secureAppsArray.add(whiteListSignature);
                }
                for (SabaMarketApp installedApp1 : installedAppArray) {
                    boolean isSabaApp = false;
                    for (String signature : secureAppsArray) {
                        if (installedApp1.getApkSig().equalsIgnoreCase(signature)) {
                            isSabaApp = true;
                        }
                    }
                    if (!isSabaApp) {
                        nonSabaApps.add(installedApp1.getApkName());
                    }
                }
                for (ApplicationInfo applicationInfo : packages) {
                    String packageName = applicationInfo.packageName;
                    String appName = applicationInfo.loadLabel(context.getPackageManager()).toString();
                    try {
                        @SuppressLint("PackageManagerGetSignatures")
                        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
                        installedPackages.add(packageInfo);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    switch (packageName) {
                        case "org.telegram.messenger":
                            dangerousAppsTemp.add(new SecurityEvent("notif", packageName, appName, null));
                            break;
                        case "com.whatsapp":
                            dangerousAppsTemp.add(new SecurityEvent("notif", packageName, appName, null));
                            break;
                        case "com.merriamwebster":
                            dangerousAppsTemp.add(new SecurityEvent("notif", packageName, appName, null));
                            break;
                        case "org.torproject.torbrowser":
                            dangerousAppsTemp.add(new SecurityEvent("notif", packageName, appName, null));
                            break;
                        case "org.torproject.android":
                            dangerousAppsTemp.add(new SecurityEvent("notif", packageName, appName, null));
                            break;
                    }
                }
                if (nonSabaApps.size() > 0) {
                    StringBuilder nonSabaAppsList = new StringBuilder();
                    for (String appName : nonSabaApps) {
                        nonSabaAppsList.append(appName).append("\n");
                    }
                    dangerousApps.add(new SecurityEvent("nonSabaApp", null, nonSabaAppsList.toString(), null));
                }

                for (SecurityEvent securityEvent : dangerousAppsTemp) {
                    dangerousApps.add(securityEvent);
                }

                for (PackageInfo packageInfo : installedPackages) {
                    boolean isSecureApp = false;
                    Signature packageSig = packageInfo.signatures[0];
                    String a = md5(packageSig.toCharsString());
                    String b = md5(String.valueOf(packageSig.hashCode()));
                    for (String sig : secureAppsArray) {
                        if (a.equalsIgnoreCase(sig)) {
                            isSecureApp = true;
                        }
                    }
                    if (!isSecureApp) {
                        nonSabaAppsPackageNames.add(packageInfo.packageName);
                    }
                }

                onTaskExecution.onTaskFinished(dangerousApps);
            }
        });
        thread.run();
    }

    // Return the list generated by above method. If you call this method prior to calling examineDangerousApps() method,
    // it will return an empty array

    public ArrayList<String> getNonSabaApps() {
        return nonSabaApps;
    }

    // This method retrieves signatures of all installed apps from applications database

    private ArrayList<SabaMarketApp> getInstalledAppsSigs(Context context) {
        ArrayList<SabaMarketApp> installedAppsSigs = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getReadableDatabase();
        Cursor cursor = sqLiteDatabase.query(INSTALLED_APPS_TABLE_NAME, INSTALLED_APP_COLUMNS, null, null, null,
                null, null);
        if (cursor.moveToFirst()) {
            do {
                installedAppsSigs.add(new SabaMarketApp(cursor.getString(cursor.getColumnIndex(INSTALLED_APP_COLUMNS[0])), cursor.getString(cursor.getColumnIndex(INSTALLED_APP_COLUMNS[1]))));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return installedAppsSigs;
    }

    // This method retreives the signatures of all SabaMarket apps

    private ArrayList<String> getSabaMarketAppsSigs(Context context) {
        ArrayList<String> fdroidAppsSigs = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = DBHelper.getInstance(context).getReadableDatabase();
        Cursor cursor = sqLiteDatabase.query(FDROID_APPS_TABLE_NAME, FDROID_APPS_COLUMNS, null, null, null,
                null, null);
        if (cursor.moveToFirst()) {
            do {
                fdroidAppsSigs.add(cursor.getString(cursor.getColumnIndex(FDROID_APPS_COLUMNS[0])));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return fdroidAppsSigs;
    }

    private static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
