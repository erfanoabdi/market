package com.sabaos.market;

import android.graphics.PixelFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import org.fdroid.fdroid.R;

// This activity is used to display a danger icon if device contains non-Saba apps and
// security level is at least basic

public class LockScreenActivity extends AppCompatActivity {


    private AppCompatActivity appCompatActivity;
    WindowManager windowManager;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_screen);
        appCompatActivity = this;
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.ic_no_entry);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        windowManager.addView(imageView, params);
    }

    @Override
    public void onPause() {
        super.onPause();
        windowManager.removeView(imageView);
        appCompatActivity.finish();
    }
 }
