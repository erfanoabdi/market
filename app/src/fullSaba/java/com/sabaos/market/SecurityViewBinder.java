package com.sabaos.market;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.sabaos.core.SabaUtils;

import org.fdroid.fdroid.BuildConfig;
import org.fdroid.fdroid.R;
import org.fdroid.fdroid.UpdateService;
import org.fdroid.fdroid.net.ConnectivityMonitorService;
import org.fdroid.fdroid.views.main.MainActivity;
import org.fdroid.fdroid.views.updates.UpdatesAdapter;

import java.util.ArrayList;

import static org.fdroid.fdroid.views.main.MainActivity.globalContext;
import static org.fdroid.fdroid.views.whatsnew.WhatsNewAdapter.appArrayList;


// Shows security events as cards

public class SecurityViewBinder implements QuickInstallCallbackListener {

    SharedPreferences sharedPreferences;


    RecyclerView securityRecyclerView;
    private static SwipeRefreshLayout securitySwipeRefresh;
    public static AppCompatActivity securityActivity;
    private static View view;
    SecurityEventsAdapter securityEventsAdapter;

    public SecurityViewBinder(final AppCompatActivity activity, FrameLayout parent) {

        view = activity.getLayoutInflater().inflate(R.layout.security_tab_layout, parent, true);
        sharedPreferences = activity.getBaseContext().getSharedPreferences("saba", Context.MODE_PRIVATE);
        if (securityActivity == null) securityActivity = activity;
        securitySwipeRefresh = view.findViewById(R.id.security_swipe_to_refresh);
        showSecurityState(view);
        securitySwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                securitySwipeRefresh.setRefreshing(false);
                UpdateService.updateNow(activity);
                showSecurityState(view);
            }
        });
    }

    public SecurityViewBinder() {

    }

    private final RecyclerView.AdapterDataObserver adapterChangeListener = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            showSecurityState(view);
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
        }
    };

    private void showSecurityState(final View view) {
        securitySwipeRefresh.setRefreshing(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final InstalledAppExaminer installedAppExaminer = new InstalledAppExaminer();
                installedAppExaminer.examineDangerousApps(securityActivity.getApplicationContext(), new OnTaskExecution() {
                    @Override
                    public void onTaskFinished(ArrayList<SecurityEvent> securityEvents) {
                        securityRecyclerView = view.findViewById(R.id.security_event_recyclerview);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(globalContext);
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        securityRecyclerView.setHasFixedSize(true);
                        securityRecyclerView.setLayoutManager(linearLayoutManager);
                        securityEventsAdapter = new SecurityEventsAdapter(generateSecurityEvents(globalContext, securityActivity, securityEvents));
                        securityRecyclerView.setAdapter(securityEventsAdapter);
                        securityRecyclerView.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                securitySwipeRefresh.setRefreshing(false);
                            }
                        }, 500);
                        if (installedAppExaminer.getNonSabaApps().size() > 0) {
                            if (BuildConfig.IS_MCI && SabaUtils.getSecurityLevel(globalContext) >= SabaUtils.SECURITY_LEVEL_BASIC) {
                                if (Build.VERSION.SDK_INT > 22) {

                                    // if you have the required permission, load the lock screen activity
                                    if (Settings.canDrawOverlays(globalContext)) {
                                        Intent intent = new Intent(globalContext, AppLockerService.class);
                                        if (Build.VERSION.SDK_INT >= 26) {
                                            globalContext.startForegroundService(intent);
                                        } else {
                                            globalContext.startService(intent);
                                        }

                                    } else {

                                        // if you don't have the required permission, show a notification which displays the security tab
                                        // of Market app when clicked
                                        NotificationManager notificationManager = (NotificationManager) globalContext.getSystemService(Context.NOTIFICATION_SERVICE);
                                        Intent intent = new Intent(globalContext, MainActivity.class);
                                        intent.putExtra("source", "security_notif");
                                        PendingIntent pendingIntent = PendingIntent.getActivity(globalContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                        if (Build.VERSION.SDK_INT >= 26) {
                                            NotificationChannel notificationChannel = new NotificationChannel(globalContext.getString(R.string.saba_market), globalContext.getString(R.string.saba_market), NotificationManager.IMPORTANCE_DEFAULT);
                                            notificationManager.createNotificationChannel(notificationChannel);
                                            Notification.Builder notification = new Notification.Builder(globalContext, globalContext.getString(R.string.saba_market))
                                                    .setSmallIcon(R.drawable.ic_saba_market)
                                                    .setLargeIcon(Icon.createWithResource(globalContext, R.drawable.ic_saba_market))
                                                    .setContentTitle(globalContext.getString(R.string.security_notif_title))
                                                    .setContentText(globalContext.getString(R.string.security_notif_description))
                                                    .setContentIntent(pendingIntent);
                                            notificationManager.notify(21, notification.build());
                                        } else {
                                            Notification notification = new Notification(R.drawable.ic_launcher_foreground, globalContext.getString(R.string.security_notif_description), System.currentTimeMillis());
                                            notificationManager.notify("Saba Maket security notification", 21, notification);
                                        }
                                    }
                                } else {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent intent1 = new Intent(globalContext, LockScreenActivity.class);
                                            globalContext.startActivity(intent1);
                                            securityActivity.finish();
                                        }
                                    }, 5000);
                                }
                            }
                        }
                    }
                });
            }
        }, 1000);
    }

    // This creates an ArrayList containing security events that are going to be displayed

    private ArrayList<SecurityEvent> generateSecurityEvents(Context context, AppCompatActivity securityActivity, ArrayList<SecurityEvent> dangerousApps) {

        ArrayList<SecurityEvent> securityEventArrayList = new ArrayList<>();

        // First we check for updates, it there is an update, we add the update card to the ArrayList
        // in order to display it to the user

        QuickInstallPreferences quickInstallPreferences = new QuickInstallPreferences(context);
        UpdatesAdapter updatesAdapter = new UpdatesAdapter(securityActivity);
        if (updatesAdapter.getItemCount() != 0) {
            securityEventArrayList.add(new SecurityEvent("updates", null, null, null));
        }

        // Then check to see if we have run the QuickInstall before, it we have, then we don't
        // need to display it anymore, if not we add it to security event ArrayList only when the device
        // is connected to a network

        if (quickInstallPreferences.getData("quickInstall") == null) {
            int netState = ConnectivityMonitorService.getNetworkState(context);
            if (netState != ConnectivityMonitorService.FLAG_NET_UNAVAILABLE) {
                if (appArrayList != null && appArrayList.size() != 0) {
                    securityEventArrayList.add(new SecurityEvent("quickInstall", context.getString(R.string.explanation), context.getString(R.string.quick_install), context.getDrawable(R.drawable.magic_icon)));
                }
            }
        }

        //Finally we check the device to dangerous apps, if there are no dangerous apps installed,
        // we just display a green card with showing that the device is secure, if not we add a card
        // to notify that there is a problem, followed by others cards indicating the problems

        if (dangerousApps.size() == 0) {
            securityEventArrayList.add(new SecurityEvent("securityLevel", context.getString(R.string.security_level_fine),
                    context.getString(R.string.security_level), context.getDrawable(R.drawable.ic_security_fine1)));

        } else {
            if (dangerousApps.size() > 0) {
                boolean Telegram = false;
                boolean WhatsApp = false;
                boolean TorBrowser = false;
                boolean TorProject = false;
                boolean MarriamWebster = false;
                for (SecurityEvent securityEvent : dangerousApps) {
                    if (securityEvent.getType().equalsIgnoreCase("notif")) {
                        switch (securityEvent.getPackageName()) {
                            case "org.telegram.messenger":
                                Telegram = true;
                                break;
                            case "com.whatsapp":
                                WhatsApp = true;
                                break;
                            case "com.merriamwebster":
                                MarriamWebster = true;
                                break;
                            case "org.torproject.torbrowser":
                                TorBrowser = true;
                                break;
                            case "org.torproject.android":
                                TorProject = true;
                                break;
                        }
                    }
                }
                if (WhatsApp && MarriamWebster && TorBrowser && TorProject && dangerousApps.size() == 4) {
                    securityEventArrayList.add(new SecurityEvent("securityLevel", context.getString(R.string.security_level_warn),
                            context.getString(R.string.security_level), context.getDrawable(R.drawable.ic_security_warn1)));
                } else {
                    securityEventArrayList.add(new SecurityEvent("securityLevel", context.getString(R.string.security_level_warn),
                            context.getString(R.string.security_level), context.getDrawable(R.drawable.ic_security_danger1)));
                }
                for (SecurityEvent securityEvent : dangerousApps) {
                    securityEventArrayList.add(securityEvent);
                }
            }
        }


        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String versionName = packageInfo.versionName;
        DBSharedPreferences sharedPreferences = new DBSharedPreferences(context);

        String aboutApp = versionName + sharedPreferences.getData("sabaRepo") + "، ویرایش ";
        securityEventArrayList.add(new SecurityEvent("aboutApp", aboutApp, null, null));

        return securityEventArrayList;
    }


    // This is called when QuickInstall Activity has successfully run

    @Override
    public void onCallbackListener() {
        if (view != null) {
            securityRecyclerView = view.findViewById(R.id.security_event_recyclerview);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(globalContext);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            securityRecyclerView.setHasFixedSize(true);
            securityRecyclerView.setLayoutManager(linearLayoutManager);
            InstalledAppExaminer installedAppExaminer = new InstalledAppExaminer();
            installedAppExaminer.examineDangerousApps(securityActivity.getApplicationContext(), new OnTaskExecution() {
                @Override
                public void onTaskFinished(ArrayList<SecurityEvent> securityEvents) {
                    securityEventsAdapter = new SecurityEventsAdapter(securityEvents);
                    securityRecyclerView.setAdapter(securityEventsAdapter);
                    securityRecyclerView.setVisibility(View.VISIBLE);
                    securitySwipeRefresh.setRefreshing(false);
                }
            });
        }
    }
}


