package com.sabaos.market;


// private class used to transfer app names and signatures of SabaMarket apps

public class SabaMarketApp {

    private String apkName;
    private String apkSig;

    public SabaMarketApp(String apkName, String apkSig) {
        this.apkName = apkName;
        this.apkSig = apkSig;
    }

    public String getApkName() {
        return apkName;
    }

    public String getApkSig() {
        return apkSig;
    }
}
