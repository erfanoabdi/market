package com.sabaos.market;

import org.fdroid.fdroid.data.App;

public class AppInstallationReport {

    private App app;
    private boolean installed;
    private int errorCode;

    public AppInstallationReport(App app, boolean installed, int errorCode) {
        this.app = app;
        this.installed =  installed;
        this.errorCode = errorCode;
    }

    public void setApp(App app) {
        this.app = app;
    }

    public void setInstalled(boolean installed) {
        this.installed = installed;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public App getApp() {
        return app;
    }

    public boolean isInstalled() {
        return installed;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
