package com.sabaos.market;

// Used to register callback to update listener

public class UpdateFinished {

    OnUpdateFinishedListener onUpdateFinishedListener;

    public void registerListener(OnUpdateFinishedListener onUpdateFinishedListener) {
        this.onUpdateFinishedListener = onUpdateFinishedListener;
    }

    public void listenToUpdateFinish() {
        onUpdateFinishedListener.onUpdateFinished();
    }
}
