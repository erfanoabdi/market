package com.sabaos.market;

import android.content.res.Configuration;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import org.fdroid.fdroid.R;

public class QuickInstallReport extends AppCompatActivity {


    public static Configuration configuration;
    private static QuickInstallReportAdapter quickInstallReportAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_install_report);
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.quick_install_actionbar);
        setTitle(getResources().getString(R.string.saba_market));
        configuration = getResources().getConfiguration();
        RecyclerView recyclerView = findViewById(R.id.quick_install_dialog_recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(linearLayoutManager);
        quickInstallReportAdapter = new QuickInstallReportAdapter();
        recyclerView.setAdapter(quickInstallReportAdapter);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickInstallCallbackListener quickInstallCallbackListener = new SecurityViewBinder();
                QuickInstallCallback quickInstallCallback = new QuickInstallCallback();
                quickInstallCallback.registerCallback(quickInstallCallbackListener);
                quickInstallCallback.callback();
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        QuickInstallCallbackListener quickInstallCallbackListener = new SecurityViewBinder();
        QuickInstallCallback quickInstallCallback = new QuickInstallCallback();
        quickInstallCallback.registerCallback(quickInstallCallbackListener);
        quickInstallCallback.callback();
        finish();
    }
}
