package com.sabaos.market;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

// Used to save type of repository used by Market app

public class DBSharedPreferences {

    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public DBSharedPreferences(Context context) {
        this.context = context;
        sharedPreferences = (android.content.SharedPreferences) context.getSharedPreferences("repo_type", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void saveData(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getData(String key) {
        return sharedPreferences.getString(key, "صفر");
    }

    public Map getAll() {
        return sharedPreferences.getAll();
    }

    public void removeData(String key) {
        editor.remove(key);
        editor.commit();
    }
}
