package com.sabaos.market;

// Called when QuickInstall is finished

public interface QuickInstallCallbackListener {

    void onCallbackListener();
}
