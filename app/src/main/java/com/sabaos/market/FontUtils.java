package com.sabaos.market;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.reflect.Field;

/**
 * Based on: https://www.coderefer.com/android-custom-font-entire-application/
 */
public class FontUtils {
    public static void setDefaultFarsiFont(Context context) {
        setDefaultFont(context, "DEFAULT", "fonts/shabnam.ttf");
        setDefaultFont(context, "MONOSPACE", "fonts/shabnam.ttf");
        setDefaultFont(context, "SERIF", "fonts/shabnam.ttf");
        setDefaultFont(context, "SANS_SERIF", "fonts/shabnam.ttf");
    }

    private static void setDefaultFont(Context context,
                                       String staticTypefaceFieldName, String fontAssetName) {
        final Typeface regular = Typeface.createFromAsset(context.getAssets(), fontAssetName);
        replaceFont(staticTypefaceFieldName, regular);
    }

    private static void replaceFont(String staticTypefaceFieldName, final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class.getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
