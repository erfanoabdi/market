/*
 * Copyright (C) 2010  Ciaran Gultnieks, ciaran@ciarang.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.fdroid.fdroid.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;

import com.sabaos.core.SabaUtils;
import com.sabaos.market.AppLockerService;
import com.sabaos.market.AppLockerUtils;
import com.sabaos.market.InstalledAppExaminer;
import com.sabaos.market.LockScreenActivity;
import com.sabaos.market.OnTaskExecution;
import com.sabaos.market.SecurityEvent;

import org.fdroid.fdroid.BuildConfig;
import org.fdroid.fdroid.R;
import org.fdroid.fdroid.UpdateService;
import org.fdroid.fdroid.Utils;
import org.fdroid.fdroid.views.main.MainActivity;

import java.util.ArrayList;

public class StartupReceiver extends BroadcastReceiver {
    private static final String TAG = "StartupReceiver";

    @Override
    public void onReceive(final Context ctx, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            UpdateService.schedule(ctx);
            if (BuildConfig.IS_MCI && SabaUtils.getSecurityLevel(ctx) >= SabaUtils.SECURITY_LEVEL_BASIC) {
                final InstalledAppExaminer installedAppExaminer = new InstalledAppExaminer();
                installedAppExaminer.examineDangerousApps(ctx, new OnTaskExecution() {
                    @Override
                    public void onTaskFinished(ArrayList<SecurityEvent> securityEvents) {

                        if (installedAppExaminer.getNonSabaApps().size() > 0) {
                            if (Build.VERSION.SDK_INT > 22) {

                                // if you have the required permission, load the lock screen activity
                                if (Settings.canDrawOverlays(ctx)) {
                                    Intent intent = new Intent(ctx, AppLockerService.class);
                                    if (Build.VERSION.SDK_INT >= 26) {
                                        ctx.startForegroundService(intent);
                                    } else {
                                        ctx.startService(intent);
                                    }
                                } else {

                                    // if you don't have the required permission, show a notification which displays the security tab
                                    // of Market app when clicked
                                    NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
                                    Intent intent = new Intent(ctx, MainActivity.class);
                                    intent.putExtra("source", "security_notif");
                                    PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                                    if (Build.VERSION.SDK_INT >= 26) {
                                        NotificationChannel notificationChannel = new NotificationChannel(ctx.getString(R.string.saba_market), ctx.getString(R.string.saba_market), NotificationManager.IMPORTANCE_DEFAULT);
                                        notificationManager.createNotificationChannel(notificationChannel);
                                        Notification.Builder notification = new Notification.Builder(ctx, ctx.getString(R.string.saba_market))
                                                .setSmallIcon(R.drawable.ic_saba_market)
                                                .setLargeIcon(Icon.createWithResource(ctx, R.drawable.ic_saba_market))
                                                .setContentTitle(ctx.getString(R.string.security_notif_title))
                                                .setContentText(ctx.getString(R.string.security_notif_description))
                                                .setContentIntent(pendingIntent);
                                        notificationManager.notify(21, notification.build());
                                    } else {
                                        Notification notification = new Notification(R.drawable.ic_launcher_foreground, ctx.getString(R.string.security_notif_description), System.currentTimeMillis());
                                        notificationManager.notify("Saba Maket security notification", 21, notification);
                                    }
                                }
                            } else {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent1 = new Intent(ctx, LockScreenActivity.class);
                                        ctx.startActivity(intent1);
                                    }
                                }, 5000);
                            }
                        }
                    }
                });
            }
        } else {
            Utils.debugLog(TAG, "received unsupported Intent " + intent);
        }
    }

}
