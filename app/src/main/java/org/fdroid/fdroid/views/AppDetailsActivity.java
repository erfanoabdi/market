/*
 * Copyright (C) 2010-12  Ciaran Gultnieks, ciaran@ciarang.com
 * Copyright (C) 2013-15 Daniel Martí <mvdan@mvdan.cc>
 * Copyright (C) 2013 Stefan Völkel, bd@bc-bd.org
 * Copyright (C) 2015 Nico Alt, nicoalt@posteo.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package org.fdroid.fdroid.views;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.database.ContentObserver;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.sabaos.market.AppInstallationReport;
import com.sabaos.market.AppInstalled;
import com.sabaos.market.BaseActivity;
import com.sabaos.market.BitmapConverter;
import com.sabaos.market.OnQuickInstallAppStatus;
import com.sabaos.market.OnAppInstalledListener;
import com.sabaos.market.QuickInstallActivity;
import com.sabaos.market.SabaSharedPreferences;

import org.fdroid.fdroid.AppUpdateStatusManager;
import org.fdroid.fdroid.FDroidApp;
import org.fdroid.fdroid.NfcHelper;
import org.fdroid.fdroid.R;
import org.fdroid.fdroid.Utils;
import org.fdroid.fdroid.data.Apk;
import org.fdroid.fdroid.data.ApkProvider;
import org.fdroid.fdroid.data.App;
import org.fdroid.fdroid.data.AppPrefsProvider;
import org.fdroid.fdroid.data.AppProvider;
import org.fdroid.fdroid.data.Schema;
import org.fdroid.fdroid.installer.InstallManagerService;
import org.fdroid.fdroid.installer.Installer;
import org.fdroid.fdroid.installer.InstallerFactory;
import org.fdroid.fdroid.installer.InstallerService;

import java.util.Iterator;

import static com.sabaos.market.QuickInstallActivity.appInstallationReports;
import static com.sabaos.market.QuickInstallActivity.appToInstall;
import static com.sabaos.market.QuickInstallActivity.installMode;
import static com.sabaos.market.QuickInstallActivity.mutex;
import static org.fdroid.fdroid.views.AppDetailsRecyclerViewAdapter.iconDrawable;

public class AppDetailsActivity extends BaseActivity
        implements ShareChooserDialog.ShareChooserDialogListener,
        AppDetailsRecyclerViewAdapter.AppDetailsRecyclerViewAdapterCallbacks {

    public static final String EXTRA_APPID = "appid";
    private static final String TAG = "AppDetailsActivity";

    private static final int REQUEST_ENABLE_BLUETOOTH = 2;
    private static final int REQUEST_PERMISSION_DIALOG = 3;
    private static final int REQUEST_UNINSTALL_DIALOG = 4;

    @SuppressWarnings("unused")
    protected BluetoothAdapter bluetoothAdapter;

    private FDroidApp fdroidApp;
    private App app;
    private RecyclerView recyclerView;
    private AppDetailsRecyclerViewAdapter adapter;
    public LocalBroadcastManager localBroadcastManager;
    private AppUpdateStatusManager.AppUpdateStatus currentStatus;
    private String type = "";
    private AppObserver appObserver;
    private AppCompatActivity activity;
    private OnQuickInstallAppStatus onQuickInstallAppStatus;

    /**
     * Check if {@code packageName} is currently visible to the user.
     */
    public static boolean isAppVisible(String packageName) {
        return packageName != null && packageName.equals(visiblePackageName);
    }

    private static String visiblePackageName;
    private static Context context;

    public AppDetailsActivity(App app, String type, Context context, AppCompatActivity activity, OnQuickInstallAppStatus onQuickInstallAppStatus) {
        this.app = app;
        this.type = type;
        this.context = context;
        this.activity = activity;
        this.onQuickInstallAppStatus = onQuickInstallAppStatus;
    }

    public AppDetailsActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        fdroidApp = (FDroidApp) getApplication();
        if (!type.equalsIgnoreCase("quickInstall")) {
            fdroidApp.applyTheme(this);
            context = getApplicationContext();
            super.onCreate(savedInstanceState);
            setContentView(R.layout.app_details2);
            setTitle(getResources().getString(R.string.app_details_activity_name));
            supportPostponeEnterTransition();
            String packageName = getPackageNameFromIntent(getIntent());
            if (!resetCurrentApp(packageName)) {
                finish();
                return;
            }
        }
        LinearLayoutManager lm = null;
        if (!type.equalsIgnoreCase("quickInstall")) {
//            bluetoothAdapter = getBluetoothAdapter();
            recyclerView = (RecyclerView) findViewById(R.id.rvDetails);
            adapter = new AppDetailsRecyclerViewAdapter(this, app, this);
            lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            lm.setStackFromEnd(false);
        }
        localBroadcastManager = LocalBroadcastManager.getInstance(context);
        // Has to be invoked after AppDetailsRecyclerViewAdapter is created.
        refreshStatus();

        if (!type.equalsIgnoreCase("quickInstall")) {
            recyclerView.setLayoutManager(lm);
            recyclerView.setAdapter(adapter);

            recyclerView.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            supportStartPostponedEnterTransition();
                            return true;
                        }
                    }
            );
        }

        if (type.equalsIgnoreCase("quickInstall")) {
            if (app != null) {
                visiblePackageName = app.packageName;
            }

            appObserver = new AppObserver(new Handler());
            context.getContentResolver().registerContentObserver(
                    AppProvider.getHighestPriorityMetadataUri(app.packageName),
                    true,
                    appObserver);

            updateNotificationsForApp();
            refreshStatus();
            registerAppStatusReceiver();
        }

    }

    private String getPackageNameFromIntent(Intent intent) {
        if (!intent.hasExtra(EXTRA_APPID)) {
            Log.e(TAG, "No package name found in the intent!");
            return null;
        }
        return intent.getStringExtra(EXTRA_APPID);
    }

    /**
     * Some notifications (like "downloading" and "installed") are not shown
     * for this app if it is open in app details.  When closing, we need to
     * refresh the notifications, so they are displayed again.
     */
    private void updateNotificationsForApp() {
        if (app != null) {
            AppUpdateStatusManager ausm = AppUpdateStatusManager.getInstance(context);
            for (AppUpdateStatusManager.AppUpdateStatus status : ausm.getByPackageName(app.packageName)) {
                if (status.status == AppUpdateStatusManager.Status.Installed) {
                    ausm.removeApk(status.getCanonicalUrl());
                } else {
                    ausm.refreshApk(status.getCanonicalUrl());
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (app != null) {
            visiblePackageName = app.packageName;
        }

        appObserver = new AppObserver(new Handler());
        getContentResolver().registerContentObserver(
                AppProvider.getHighestPriorityMetadataUri(app.packageName),
                true,
                appObserver);

        updateNotificationsForApp();
        refreshStatus();
        registerAppStatusReceiver();
    }

    /**
     * Figures out the current install/update/download/etc status for the app we are viewing.
     * Then, asks the view to update itself to reflect this status.
     */
    private void refreshStatus() {
        AppUpdateStatusManager ausm = AppUpdateStatusManager.getInstance(context);
        Iterator<AppUpdateStatusManager.AppUpdateStatus> statuses = ausm.getByPackageName(app.packageName).iterator();
        if (statuses.hasNext()) {
            AppUpdateStatusManager.AppUpdateStatus status = statuses.next();
            updateAppStatus(status, false);
        }

        currentStatus = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterAppStatusReceiver();
    }

    protected void onStop() {
        super.onStop();
        visiblePackageName = null;

        getContentResolver().unregisterContentObserver(appObserver);

        // When leaving the app details, make sure to refresh app status for this app, since
        // we might want to show notifications for it now.
        updateNotificationsForApp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean ret = super.onCreateOptionsMenu(menu);
        if (ret) {
            getMenuInflater().inflate(R.menu.details2, menu);
        }
        return ret;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (app == null) {
            return true;
        }
        MenuItem itemIgnoreAll = menu.findItem(R.id.action_ignore_all);
        if (itemIgnoreAll != null) {
            itemIgnoreAll.setChecked(app.getPrefs(this).ignoreAllUpdates);
        }
        MenuItem itemIgnoreThis = menu.findItem(R.id.action_ignore_this);
        if (itemIgnoreThis != null) {
            itemIgnoreThis.setVisible(app.hasUpdates());
            itemIgnoreThis.setChecked(app.getPrefs(this).ignoreThisUpdate >= app.suggestedVersionCode);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_share) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, app.name);
            shareIntent.putExtra(Intent.EXTRA_TEXT, app.name + " (" + app.summary
                    + ") - https://f-droid.org/app/" + app.packageName);

            boolean showNearbyItem = app.isInstalled(getApplicationContext()) && bluetoothAdapter != null;
            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.rootCoordinator);
            ShareChooserDialog.createChooser(coordinatorLayout, this, this, shareIntent, showNearbyItem);
            return true;
        } else if (item.getItemId() == R.id.action_ignore_all) {
            app.getPrefs(this).ignoreAllUpdates ^= true;
            item.setChecked(app.getPrefs(this).ignoreAllUpdates);
            AppPrefsProvider.Helper.update(this, app, app.getPrefs(this));
            return true;
        } else if (item.getItemId() == R.id.action_ignore_this) {
            if (app.getPrefs(this).ignoreThisUpdate >= app.suggestedVersionCode) {
                app.getPrefs(this).ignoreThisUpdate = 0;
            } else {
                app.getPrefs(this).ignoreThisUpdate = app.suggestedVersionCode;
            }
            item.setChecked(app.getPrefs(this).ignoreThisUpdate > 0);
            AppPrefsProvider.Helper.update(this, app, app.getPrefs(this));
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNearby() {
        // If Bluetooth has not been enabled/turned on, then
        // enabling device discoverability will automatically enable Bluetooth
        Intent discoverBt = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverBt.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 121);
        startActivityForResult(discoverBt, REQUEST_ENABLE_BLUETOOTH);
        // if this is successful, the Bluetooth transfer is started
    }

    @Override
    public void onResolvedShareIntent(Intent shareIntent) {
        startActivity(shareIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BLUETOOTH:
                fdroidApp.sendViaBluetooth(this, resultCode, app.packageName);
                break;
            case REQUEST_PERMISSION_DIALOG:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    Apk apk = ApkProvider.Helper.findByUri(context, uri, Schema.ApkTable.Cols.ALL);
                    InstallManagerService.queue(context, app, apk);

                }
                break;
            case REQUEST_UNINSTALL_DIALOG:
                if (resultCode == Activity.RESULT_OK) {
                    startUninstall();
                }
                break;
        }
    }

    @Override
    public void installApk() {
        Apk apkToInstall = ApkProvider.Helper.findSuggestedApk(this, app);
        installApk(apkToInstall);
    }


    // Install the version of this app denoted by 'app.curApk'.
    @Override
    public void installApk(final Apk apk) {

        int isWebAppInd = app.description.indexOf("وب اپ ");
        boolean isWebApp = isWebAppInd > 0 && isWebAppInd < 30;
        if (Build.VERSION.SDK_INT >= 26 && app.webSite != null && isWebApp) {
            installWebApp(app);
        } else {
            if (type.equalsIgnoreCase("quickInstall")) {
                mutex = "apk";
            }
            if (isFinishing()) {
                return;
            }

            if (!apk.compatible) {
                AlertDialog.Builder builder;
                if (activity != null) {
                    builder = new AlertDialog.Builder(activity);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setMessage(R.string.installIncompatible);
                builder.setPositiveButton(R.string.yes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                initiateInstall(apk);

                            }
                        });
                builder.setNegativeButton(R.string.no,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                AppInstalled appInstalled = new AppInstalled();
                                OnAppInstalledListener onAppInstalledListener = new QuickInstallActivity();
                                appInstalled.registerListener(onAppInstalledListener);
                                appInstalled.listenToInstalls();
                                if (type.equalsIgnoreCase("quickInstall")) {
                                    for (AppInstallationReport appInstallationReport : appInstallationReports) {
                                        if (appInstallationReport.getApp().name.equalsIgnoreCase(appToInstall.name)) {
                                            appInstallationReport.setInstalled(false);
                                            appInstallationReport.setErrorCode(1);
                                            break;
                                        }
                                    }
                                }
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return;
            }
            if (app.installedSig != null && apk.sig != null
                    && !apk.sig.equals(app.installedSig)) {
                AlertDialog.Builder builder;
                if (activity != null) {
                    builder = new AlertDialog.Builder(activity);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setMessage(R.string.SignatureMismatch).setPositiveButton(
                        R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                AppInstalled appInstalled = new AppInstalled();
                                OnAppInstalledListener onAppInstalledListener = new QuickInstallActivity();
                                appInstalled.registerListener(onAppInstalledListener);
                                appInstalled.listenToInstalls();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return;
            }
            initiateInstall(apk);

            // Scroll back to the header, so that the user can see the progress beginning. This can be
            // removed once https://gitlab.com/fdroid/fdroidclient/issues/903 is implemented. However
            // for now it adds valuable feedback to the user about the download they just initiated.
            if (!type.equalsIgnoreCase("quickInstall")) {
                ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(0, 0);
            }
        }
    }

    private void initiateInstall(Apk apk) {
        Installer installer = InstallerFactory.create(context, apk);
        Intent intent = installer.getPermissionScreen();
        if (intent != null) {
            // permission screen required
            Utils.debugLog(TAG, "permission screen required");
            if (activity != null) {
                activity.startActivityForResult(intent, REQUEST_PERMISSION_DIALOG);
            } else {
                startActivityForResult(intent, REQUEST_PERMISSION_DIALOG);
            }
            return;
        }

        InstallManagerService.queue(context, app, apk);
    }

    private void startUninstall() {
        registerUninstallReceiver();
        InstallerService.uninstall(this, app.installedApk);
    }

    private void registerUninstallReceiver() {
        localBroadcastManager.registerReceiver(uninstallReceiver,
                Installer.getUninstallIntentFilter(app.packageName));
    }

    private void unregisterUninstallReceiver() {
        localBroadcastManager.unregisterReceiver(uninstallReceiver);
    }

    private void registerAppStatusReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(AppUpdateStatusManager.BROADCAST_APPSTATUS_REMOVED);
        filter.addAction(AppUpdateStatusManager.BROADCAST_APPSTATUS_ADDED);
        filter.addAction(AppUpdateStatusManager.BROADCAST_APPSTATUS_CHANGED);
        localBroadcastManager.registerReceiver(appStatusReceiver, filter);
    }

    private void unregisterAppStatusReceiver() {
        localBroadcastManager.unregisterReceiver(appStatusReceiver);
    }

    private void unregisterInstallReceiver() {
        localBroadcastManager.unregisterReceiver(installReceiver);
    }

    private void updateAppStatus(@Nullable AppUpdateStatusManager.AppUpdateStatus newStatus, boolean justReceived) {
        this.currentStatus = newStatus;
        if (this.currentStatus == null) {
            return;
        }

        switch (newStatus.status) {
            case PendingInstall:
            case Downloading:
                if (!type.equalsIgnoreCase("quickInstall")) {
                    if (newStatus.progressMax == 0) {
                        // The first progress notification we get telling us our status is "Downloading"
                        adapter.notifyAboutDownloadedApk(newStatus.apk);
                        adapter.setIndeterminateProgress(R.string.download_pending);
                    } else {
                        adapter.setProgress(newStatus.progressCurrent, newStatus.progressMax);
                    }
                }
                if (type.equalsIgnoreCase("quickInstall")) {
                    onQuickInstallAppStatus.onDownload();
                }
                break;

            case ReadyToInstall:
                if (!type.equalsIgnoreCase("quickInstall")) {
                    if (justReceived) {
                        adapter.setIndeterminateProgress(R.string.installing);
                        localBroadcastManager.registerReceiver(installReceiver,
                                Installer.getInstallIntentFilter(newStatus.getCanonicalUrl()));
                    }
                }
                if (justReceived) {
                    localBroadcastManager.registerReceiver(installReceiver,
                            Installer.getInstallIntentFilter(newStatus.getCanonicalUrl()));
                }
                break;

            case DownloadInterrupted:
                if (!type.equalsIgnoreCase("quickInstall")) {
                    if (justReceived) {
                        if (TextUtils.isEmpty(newStatus.errorText)) {
                            Toast.makeText(this, R.string.details_notinstalled, Toast.LENGTH_LONG).show();
                        } else {
                            String msg = newStatus.errorText;
                            if (!newStatus.getCanonicalUrl().equals(msg))
                                msg += " " + newStatus.getCanonicalUrl();
                            Toast.makeText(this, R.string.download_error, Toast.LENGTH_SHORT).show();
                            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                        }

                        adapter.clearProgress();
                    }
                }
                if (type.equalsIgnoreCase("quickInstall")) {
                    onQuickInstallAppStatus.onDownloadError();
                }
                break;

            case Installing:
                if (!type.equalsIgnoreCase("quickInstall")) {
                    adapter.setIndeterminateProgress(R.string.installing);
                }
                if (type.equalsIgnoreCase("quickInstall")) {
                    onQuickInstallAppStatus.onInstall();
                }
                break;

            case Installed:
                if (type.equalsIgnoreCase("quickInstall")) {
                    onQuickInstallAppStatus.onInstallComplete();
                }
            case UpdateAvailable:
            case InstallError:
                if (type.equalsIgnoreCase("quickInstall")) {
                    onQuickInstallAppStatus.onInstallError();
                }
                // Ignore.
                break;
        }

    }

    private final BroadcastReceiver appStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            AppUpdateStatusManager.AppUpdateStatus status = intent.getParcelableExtra(
                    AppUpdateStatusManager.EXTRA_STATUS);
            if (type.equalsIgnoreCase("quickInstall")) {
                updateAppStatus(status, true);
                return;
            }
            boolean isRemoving = TextUtils.equals(intent.getAction(),
                    AppUpdateStatusManager.BROADCAST_APPSTATUS_REMOVED);
            if (currentStatus != null
                    && isRemoving
                    && !TextUtils.equals(status.getCanonicalUrl(), currentStatus.getCanonicalUrl())) {
                Utils.debugLog(TAG, "Ignoring app status change because it belongs to "
                        + status.getCanonicalUrl() + " not " + currentStatus.getCanonicalUrl());
            } else if (status != null && !TextUtils.equals(status.apk.packageName, app.packageName)) {
                Utils.debugLog(TAG, "Ignoring app status change because it belongs to "
                        + status.apk.packageName + " not " + app.packageName);
            } else {
                updateAppStatus(status, true);
            }
        }
    };

    private final BroadcastReceiver installReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean callback = false;
            try {
                switch (intent.getAction()) {
                    case Installer.ACTION_INSTALL_STARTED:
                        if (!type.equalsIgnoreCase("quickInstall")) {
                            adapter.setIndeterminateProgress(R.string.installing);
                        }
                        break;
                    case Installer.ACTION_INSTALL_COMPLETE:
                        if (!type.equalsIgnoreCase("quickInstall")) {
                            adapter.clearProgress();
                        }
                        unregisterInstallReceiver();
                        // Ideally, we wouldn't try to update the view here, because the InstalledAppProviderService
                        // hasn't had time to do its thing and mark the app as installed. Instead, we
                        // wait for that s|ervice to notify us, and then we will respond in appObserver.

                        // Having said that, there are some cases where the PackageManager doesn't
                        // return control back to us until after it has already braodcast to the
                        // InstalledAppProviderService. This means that we are not listening for any
                        // feedback from InstalledAppProviderService (we intentionally stop listening in
                        // onPause). Empirically, this happens when upgrading an app rather than a clean
                        // install. However given the nature of this race condition, it may be different
                        // on different operating systems. As such, we'll just update our view now. It may
                        // happen again in our appObserver, but that will only cause a little more load
                        // on the system, it shouldn't cause a different UX.
                        onAppChanged();
                        // after doing the broadcasts, finish this transparent wrapper activity
                        // after doing the broadcasts, finish this transparent wrapper activity
                        callback = true;
                        if (type.equalsIgnoreCase("quickInstall")) {
                            for (AppInstallationReport appInstallationReport : appInstallationReports) {
                                if (appInstallationReport.getApp().name.equalsIgnoreCase(appToInstall.name)) {
                                    appInstallationReport.setInstalled(true);
                                    appInstallationReport.setErrorCode(0);
                                    break;
                                }
                            }
                        }
                        break;
                    case Installer.ACTION_INSTALL_INTERRUPTED:
                        if (!type.equalsIgnoreCase("quickInstall")) {
                            adapter.clearProgress();
                            onAppChanged();

                            String errorMessage =
                                    intent.getStringExtra(Installer.EXTRA_ERROR_MESSAGE);

                            if (!TextUtils.isEmpty(errorMessage) && !isFinishing()) {
                                Log.e(TAG, "install aborted with errorMessage: " + errorMessage);

                                String title = String.format(
                                        getString(R.string.install_error_notify_title),
                                        app.name);

                                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AppDetailsActivity.this);
                                alertBuilder.setTitle(title);
                                alertBuilder.setMessage(errorMessage);
                                alertBuilder.setNeutralButton(android.R.string.ok, null);
                                alertBuilder.create().show();
                            }
                        }
                        unregisterInstallReceiver();
                        // after doing the broadcasts, finish this transparent wrapper activity
                        callback = true;
                        if (type.equalsIgnoreCase("quickInstall")) {
                            for (AppInstallationReport appInstallationReport : appInstallationReports) {
                                if (appInstallationReport.getApp().name.equalsIgnoreCase(appToInstall.name)) {
                                    appInstallationReport.setInstalled(false);
                                    appInstallationReport.setErrorCode(1);
                                    break;
                                }
                            }
                        }
                        break;
                    case Installer.ACTION_INSTALL_USER_INTERACTION:
                        Apk apk = intent.getParcelableExtra(Installer.EXTRA_APK);
                        if (!isAppVisible(apk.packageName)) {
                            Utils.debugLog(TAG, "Ignore request for user interaction from installer, because "
                                    + apk.packageName + " is no longer showing.");
                            break;
                        }

                        Utils.debugLog(TAG, "Automatically showing package manager for " + apk.packageName
                                + " as it is being viewed by the user.");
                        PendingIntent pendingIntent = intent.getParcelableExtra(Installer.EXTRA_USER_INTERACTION_PI);

                        try {
                            pendingIntent.send();
                        } catch (PendingIntent.CanceledException e) {
                            Log.e(TAG, "PI canceled", e);
                        }

                        break;
                    default:
                        throw new RuntimeException("intent action not handled!");
                }
            } catch (RuntimeException e) {
                e.printStackTrace();
                // after doing the broadcasts, finish this transparent wrapper activity
            } finally {
                if (callback) {
                    if (!installMode.equalsIgnoreCase("defaultInstaller")) {
                        AppInstalled appInstalled = new AppInstalled();
                        OnAppInstalledListener onAppInstalledListener = new QuickInstallActivity();
                        appInstalled.registerListener(onAppInstalledListener);
                        appInstalled.listenToInstalls();
                        installMode = "";
                    }
                }
            }
        }
    };

    private final BroadcastReceiver uninstallReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case Installer.ACTION_UNINSTALL_STARTED:
                    adapter.setIndeterminateProgress(R.string.uninstalling);
                    break;
                case Installer.ACTION_UNINSTALL_COMPLETE:
                    adapter.clearProgress();
                    onAppChanged();
                    unregisterUninstallReceiver();
                    break;
                case Installer.ACTION_UNINSTALL_INTERRUPTED:
                    adapter.clearProgress();
                    String errorMessage =
                            intent.getStringExtra(Installer.EXTRA_ERROR_MESSAGE);

                    if (!TextUtils.isEmpty(errorMessage)) {
                        Log.e(TAG, "uninstall aborted with errorMessage: " + errorMessage);

                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AppDetailsActivity.this);
                        Uri uri = intent.getData();
                        if (uri == null) {
                            alertBuilder.setTitle(getString(R.string.uninstall_error_notify_title, ""));
                        } else {
                            alertBuilder.setTitle(getString(R.string.uninstall_error_notify_title,
                                    uri.getSchemeSpecificPart()));
                        }
                        alertBuilder.setMessage(errorMessage);
                        alertBuilder.setNeutralButton(android.R.string.ok, null);
                        alertBuilder.create().show();
                    }
                    unregisterUninstallReceiver();
                    break;
                case Installer.ACTION_UNINSTALL_USER_INTERACTION:
                    PendingIntent uninstallPendingIntent =
                            intent.getParcelableExtra(Installer.EXTRA_USER_INTERACTION_PI);

                    try {
                        uninstallPendingIntent.send();
                    } catch (PendingIntent.CanceledException e) {
                        Log.e(TAG, "PI canceled", e);
                    }

                    break;
                default:
                    throw new RuntimeException("intent action not handled!");
            }
        }
    };

    /**
     * Reset the display and list contents. Used when entering the activity, and
     * also when something has been installed/uninstalled.  An index update or
     * other external factors might have changed since {@code app} was set
     * before.  This also removes all pending installs with
     * {@link AppUpdateStatusManager.Status#Installed Installed}
     * status for this {@code packageName}, to prevent any lingering open ones from
     * messing up any action that the user might take.  They sometimes might not get
     * removed while F-Droid was in the background.
     * <p>
     * Shows a {@link Toast} if no {@link App} was found matching {@code packageName}.
     *
     * @return whether the {@link App} for a given {@code packageName} is still available
     */
    private boolean resetCurrentApp(String packageName) {

        if (TextUtils.isEmpty(packageName)) {
            return false;
        }
        app = AppProvider.Helper.findHighestPriorityMetadata(context.getContentResolver(), packageName);

        //
        AppUpdateStatusManager ausm = AppUpdateStatusManager.getInstance(context);
        for (AppUpdateStatusManager.AppUpdateStatus status : ausm.getByPackageName(packageName)) {
            if (status.status == AppUpdateStatusManager.Status.Installed) {
                ausm.removeApk(status.getCanonicalUrl());
            }
        }
        if (app == null) {
            Toast.makeText(context, R.string.no_such_app, Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private void onAppChanged() {
        if (!type.equalsIgnoreCase("quickInstall")) {
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    String packageName = app != null ? app.packageName : null;
                    if (!resetCurrentApp(packageName)) {
                        AppDetailsActivity.this.finish();
                        return;
                    }
                    AppDetailsRecyclerViewAdapter adapter = (AppDetailsRecyclerViewAdapter) recyclerView.getAdapter();
                    adapter.updateItems(app);
                    refreshStatus();
                    supportInvalidateOptionsMenu();
                }
            });
        } else {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    String packageName = app != null ? app.packageName : null;
                    if (!resetCurrentApp(packageName)) {
                        AppDetailsActivity.this.finish();
                        return;
                    }
                    refreshStatus();
                }
            };
            runnable.run();
        }
    }

    @Override
    public boolean isAppDownloading() {
        return currentStatus != null &&
                (currentStatus.status == AppUpdateStatusManager.Status.PendingInstall
                        || currentStatus.status == AppUpdateStatusManager.Status.Downloading);
    }

    @Override
    public void enableAndroidBeam() {
        NfcHelper.setAndroidBeam(this, app.packageName);
    }

    @Override
    public void disableAndroidBeam() {
        NfcHelper.disableAndroidBeam(this);
    }

    @TargetApi(18)
    private BluetoothAdapter getBluetoothAdapter() {
        // to use the new, recommended way of getting the adapter
        // http://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html
        if (Build.VERSION.SDK_INT < 18) {
            return BluetoothAdapter.getDefaultAdapter();
        }
        return ((BluetoothManager) getSystemService(BLUETOOTH_SERVICE)).getAdapter();
    }

    @Override
    public void openUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        if (intent.resolveActivity(getPackageManager()) == null) {
            Toast.makeText(context,
                    getString(R.string.no_handler_app, intent.getDataString()),
                    Toast.LENGTH_LONG).show();
            return;
        }
        startActivity(intent);
    }

    @Override
    public void installCancel() {
        if (currentStatus != null) {
            InstallManagerService.cancel(this, currentStatus.getCanonicalUrl());
        }
    }

    @Override
    public void launchApk() {
        Intent intent = getPackageManager().getLaunchIntentForPackage(app.packageName);
        if (intent != null) {
            startActivity(intent);
        } else {
            // This can happen when the app was just uninstalled.
            Toast.makeText(context, R.string.app_not_installed, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Uninstall the app from the current screen.  Since there are many ways
     * to uninstall an app, including from Google Play, {@code adb uninstall},
     * or Settings -> Apps, this method cannot ever be sure that the app isn't
     * already being uninstalled.  So it needs to check that we can actually
     * get info on the installed app, otherwise, just call it interrupted and
     * quit.
     *
     * @see <a href="https://gitlab.com/fdroid/fdroidclient/issues/1435">issue #1435</a>
     */
    @Override
    public void uninstallApk() {
        Apk apk = app.installedApk;
        if (apk == null) {
            apk = app.getMediaApkifInstalled(getApplicationContext());
            if (apk == null) {
                // When the app isn't a media file - the above workaround refers to this.
                apk = app.getInstalledApk(this);
                if (apk == null) {
                    Log.d(TAG, "Couldn't find installed apk for " + app.packageName);
                    Toast.makeText(this, R.string.uninstall_error_unknown, Toast.LENGTH_SHORT).show();
                    uninstallReceiver.onReceive(this, new Intent(Installer.ACTION_UNINSTALL_INTERRUPTED));
                    return;
                }
            }
            app.installedApk = apk;
        }
        Installer installer = InstallerFactory.create(this, apk);
        Intent intent = installer.getUninstallScreen();
        if (intent != null) {
            // uninstall screen required
            Utils.debugLog(TAG, "screen screen required");
            startActivityForResult(intent, REQUEST_UNINSTALL_DIALOG);
            return;
        }
        startUninstall();
    }

    // observer to update view when package has been installed/deleted


    class AppObserver extends ContentObserver {

        AppObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            onAppChanged();
        }

    }

    public void installWebApp(final App app) {
        mutex = "webApp";
        SabaSharedPreferences sabaSharedPreferences = new SabaSharedPreferences(context);
        if (sabaSharedPreferences.getData(app.name) == null) {
            ShortcutManager shortcutManager = context.getSystemService(ShortcutManager.class);
            switch (app.packageName) {
                case "cab.snapp.passenger" : iconDrawable = context.getDrawable(R.drawable.ic_snap); break;
                case "taxi.tap30.passenger" : iconDrawable = context.getDrawable(R.drawable.ic_tap30); break;
                case "com.instagram.android" : iconDrawable = context.getDrawable(R.drawable.ic_instagram); break;
//                case "ir.torob" : iconDrawable = context.getDrawable(R.drawable.ic_torob); break;

                default: iconDrawable = context.getDrawable(R.drawable.images);
            }
            if (shortcutManager.isRequestPinShortcutSupported()) {
                ShortcutInfo shortcut = new ShortcutInfo.Builder(context, app.name)
                        .setShortLabel(app.name)
                        .setLongLabel(app.summary)
                        .setIcon(Icon.createWithBitmap(BitmapConverter.drawableToBitmap(iconDrawable)))
                        .setIntent(new Intent(Intent.ACTION_VIEW, Uri.parse(app.webSite)))
                        .build();


                Intent pinnedShortcutCallbackIntent = shortcutManager.createShortcutResultIntent(shortcut);

                PendingIntent successCallback = PendingIntent.getBroadcast(context, 0,
                        pinnedShortcutCallbackIntent, 0);
                shortcutManager.requestPinShortcut(shortcut, successCallback.getIntentSender());
            } else {
                Intent intent = getPackageManager().getLaunchIntentForPackage(app.packageName);
                if (intent != null) {
                    context.startActivity(intent);
                } else {
                    // This can happen when the app was just uninstalled.
                    Toast.makeText(context, R.string.app_not_installed, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

}